---------------------------------------------------------------------------------------------------
-- func: giveset2 <player> <set name>
-- desc: Gives a set to the target player.
-- courtesy of coreyms/topaz
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 5,
    parameters = "ss"
}

function onTrigger(player, target, setName)

	if(target == nil) then
		player:PrintToPlayer("You must enter a valid player name and set name.")
		player:PrintToPlayer("!giveset2 <player> <set name>")
		player:PrintToPlayer("For list of items, do: !giveset2 list")
		return
	end

    if (setName == nil) then
		if (target == "list") then
			player:PrintToPlayer("war35, THF75evasion, RDM75enfeb, test")
		--	player:PrintToPlayer("shadowmelee, valkyriemelee, crimson, blood, enkidu, oracle, aurum, homam, nashira, hachiryu, askar, skygod, nocturnus,")
		--	player:PrintToPlayer("ares, skadi, usukane, marduk, morrigan, amir, pahluwan, yigit, valhalla, mahatma, goliard, blessed, blessed1, dusk, dusk1")
		else
			player:PrintToPlayer("You must enter a valid player name and set name.")
			player:PrintToPlayer("!giveset2 <player> <set name>")
			player:PrintToPlayer("For list of items, do: !giveset2 list")
        end
		return
    end

    local targ = GetPlayerByName( target )
    if (targ == nil) then
        player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) )
        return
    end

	local setGiven = true;

    -- Attempt to give the target the item..
    if (targ:getFreeSlotsCount() < 15) then
        player:PrintToPlayer( string.format( "Player '%s' does not have free space for those items!", target ) )
    else
		if (setName == "war35") then
			 targ:addItem(12486) -- Emperor Hairpin
			 targ:addItem(12566) -- Centurion Scale Mail
			 targ:addItem(12761) -- Custom M Gloves
			 targ:addItem(13014) -- Leaping Boots
			 targ:addItem(13015) -- Custom M Boots
			 targ:addItem(13061) -- Spike Necklace
			 targ:addItem(13225) -- Brave Belt
			 targ:addItem(13326) -- Beetle Earring +1
			 targ:addItem(13326) -- Beetle Earring +1
			 targ:addItem(13522) -- Courage Ring
			 targ:addItem(13522) -- Courage Ring
			 targ:addItem(13631) -- Nomad's Mantle
			 targ:addItem(14260) -- Republic Subligar
			 targ:addItem(16706) -- Heavy Axe
			 targ:addItem(19009) -- Brass Grip
		elseif (setName == "THF75evasion") then
        	 targ:addItem(13734) -- scorpion_harness_+1
        	 targ:addItem(14813) -- brutal_earring
        	 targ:addItem(14739) -- suppanomimi
        	 targ:addItem(13056) -- peacock_charm
        	 targ:addItem(13189) -- speed_belt
        	 targ:addItem(15373) -- bravos_subligar
        	 targ:addItem(15107) -- assassins_armlets
        	 targ:addItem(15477) -- boxers_mantle
        	 targ:addItem(15543) -- rajas_ring
        	 targ:addItem(13281) -- speed_belt
        	 targ:addItem(13734) -- snipers_ring_+1
        	 targ:addItem(13014) -- leaping_boots
        	 targ:addItem(13915) -- optical_hat
        	 targ:addItem(18141) -- ungur_boomerang
        	 targ:addItem(16480) -- thiefs_knife
        	 targ:addItem(18018) -- sirocco_kukri
		elseif (setName == "RDM75enfeb") then
        	 targ:addItem(17558) -- Apollo's staff
        	 targ:addItem(15076) -- Duelist's Chapeau
        	 targ:addItem(13155) -- Enfeebling Torque
        	 targ:addItem(14812) -- Loquacious earring
        	 targ:addItem(14724) -- Moldavite earring
        	 targ:addItem(12642) -- Warlock's Tabard
        	 targ:addItem(14833) -- Marine M gloves
        	 targ:addItem(15545) -- Tamas Ring
        	 targ:addItem(13301) -- Vivian Ring
        	 targ:addItem(13627) -- Prism Cape
        	 targ:addItem(15873) -- Duelist's Belt
        	 targ:addItem(14301) -- Errant Slops
        	 targ:addItem(15136) -- Duelist's Boots
        	 targ:addItem(19018) -- Bugard Strap +1
        	 targ:addItem(18140) -- phantom_tathlum
		elseif (setName == "test") then
			 targ:addItem(16119)
		else
			setGiven = false;
		end
		if(setGiven) then
			player:PrintToPlayer( string.format( "Gave player '%s' the set '%s' ", target, setName ) )
		else
			player:PrintToPlayer("You must specify a valid set name, !giveset2 list")
		end
    end
end