-----------------------------------
-- Ability: Hunter's Roll
-- Enhances accuracy and ranged accuracy for party members within area of effect
-- Optimal Job: Ranger
-- Lucky Number: 4
-- Unlucky Number: 8
-- Level: 11
-- Phantom Roll +1 Value: 5
--
-- Die Roll    |Without RNG |With RNG
-- --------    ------------ -------
-- 1           |+10         |+25
-- 2           |+13         |+28
-- 3           |+15         |+30
-- 4           |+40         |+55
-- 5           |+18         |+33
-- 6           |+20         |+35
-- 7           |+25         |+40
-- 8           |+5          |+20
-- 9           |+27         |+42
-- 10          |+30         |+45
-- 11          |+50         |+65
-- Bust        |-5          |-5
-----------------------------------
require("scripts/globals/settings")
require("scripts/globals/ability")
require("scripts/globals/status")
require("scripts/globals/msg")
-----------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)
	local roll = math.random(1, 11)
	local power = {10, 13, 15, 40, 18, 20, 25, 5, 27, 30, 50}
	local potency = power[roll]
	
	if target:hasStatusEffect(tpz.effect.HUNTERS_ROLL) then
		return
	else
	MobBuffMove(mob, tpz.effect.HUNTERS_ROLL, potency, 0, 180)
	end
	
	skill:setMsg(tpz.msg.basic.ROLL_MAIN)
	
	return roll
end