---------------------------------------------
-- Choke Breath
-- Description: Deals damage to players within a fan-shaped area originating from the caster. Inflicts Paralysis and Silence.
-- Type: Physical
-- Utsusemi/Blink absorb:  2-3 shadows
-- Range: Unknown radial
---------------------------------------------
require("scripts/globals/monstertpmoves")
require("scripts/globals/settings")
require("scripts/globals/status")
---------------------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)

    local rand = math.random(1,2)
    if rand == 1 and mob:getFamily() == 141 then -- Hoof Volley ability only used by NMs
        if (skill:getID() ~= 1330) then
        mob:useMobAbility(1330)
       end
    end

    local numhits = math.random(1, 3)
    local dmgmod = MobBreathMove(mob, target, 0.15, 0.25, tpz.magic.ele.WIND, 200)
    local dmg = MobFinalAdjustments(dmgmod, mob, skill, target, tpz.attackType.BREATH, tpz.damageType.WIND, numhits)
    target:takeDamage(dmg, mob, tpz.attackType.BREATH, tpz.damageType.WIND)

    MobStatusEffectMove(mob, target, tpz.effect.PARALYSIS, 15, 0, 60)
    MobStatusEffectMove(mob, target, tpz.effect.SILENCE, 1, 0, 60)

    return dmg

end
