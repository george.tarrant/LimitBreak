-----------------------------------
-- Ability: Ninja Roll
-- Enhances evasion for party members within area of effect
-- Optimal Job: Ninja
-- Lucky Number: 4
-- Unlucky Number: 8
-- Jobs:
-- Corsair Level 8
-- Phantom Roll +1 Value: 2
--
-- Die Roll    |With NIN
-- --------    ----------
-- 1           |+4
-- 2           |+6
-- 3           |+8
-- 4           |+25
-- 5           |+10
-- 6           |+12
-- 7           |+14
-- 8           |+2
-- 9           |+17
-- 10          |+20
-- 11          |+30
-- Bust        |-10
-----------------------------------
require("scripts/globals/settings")
require("scripts/globals/ability")
require("scripts/globals/status")
require("scripts/globals/msg")
-----------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)
	local roll = math.random(1, 11)
	local power = {4, 6, 8, 25, 10, 12, 14, 2, 17, 20, 30}
	local potency = power[roll]
	
	if target:hasStatusEffect(tpz.effect.NINJA_ROLL) then
		return
	else
	MobBuffMove(mob, tpz.effect.NINJA_ROLL, potency, 0, 180)
	end
	
	skill:setMsg(tpz.msg.basic.ROLL_MAIN)
	
	return roll
end