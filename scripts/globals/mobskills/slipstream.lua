---------------------------------------------
-- Slipstream
-- Accuracy down on target.
-- Type: Enfeebling
-- Range: Radial
---------------------------------------------
require("scripts/globals/monstertpmoves")
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/msg")
---------------------------------------------

function onMobSkillCheck(target, mob, skill)
    -- not used in Uleguerand_Range, Toraimarai_Canal, Qufim_Island, Upper_Delkfutts_Tower, Den_of_Rancor, Garlaige_Citadel
    if (mob:getZoneID() == 200) or  (mob:getZoneID() == 5) or (mob:getZoneID() ==169) or (mob:getZoneID() == 126) or (mob:getZoneID() == 158) (mob:getZoneID() == 160) or (target:isMobType(MOBTYPE_NOTORIOUS)) then
        return 1
    end
        return 0
end

function onMobWeaponSkill(target, mob, skill)
    local typeEffect = tpz.effect.ACCURACY_DOWN

    skill:setMsg(MobStatusEffectMove(mob, target, typeEffect, 40, 0, 120))

    return typeEffect
end