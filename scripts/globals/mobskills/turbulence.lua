---------------------------------------------
--  Turbulence
--  Description: Deals wind damage to enemies within area of effect.
--  Type: Magical Wind (Element)
--  Range: Radial
---------------------------------------------
require("scripts/globals/monstertpmoves")
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/msg")
---------------------------------------------

function onMobSkillCheck(target, mob, skill)
    -- not used in Uleguerand_Range, Toraimarai_Canal, Qufim_Island, Upper_Delkfutts_Tower, Den_of_Rancor, Garlaige_Citadel
    if (mob:getZoneID() == 200) or  (mob:getZoneID() == 5) or (mob:getZoneID() ==169) or (mob:getZoneID() == 126) or (mob:getZoneID() == 158) (mob:getZoneID() == 160) or (target:isMobType(MOBTYPE_NOTORIOUS)) then
        return 1
    end
        return 0
end

function onMobWeaponSkill(target, mob, skill)
    local dmgmod = 1
    local accmod = 1
    local info = MobMagicalMove(mob, target, skill, mob:getWeaponDmg()*3.0, tpz.magic.ele.WIND, dmgmod, TP_NO_EFFECT)
    local dmg = MobFinalAdjustments(info.dmg, mob, skill, target, tpz.attackType.MAGICAL, tpz.damageType.WIND, MOBPARAM_WIPE_SHADOWS)
    target:takeDamage(dmg, mob, tpz.attackType.MAGICAL, tpz.damageType.WIND)
    return dmg
end