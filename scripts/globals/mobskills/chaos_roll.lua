-----------------------------------
-- Ability: Chaos Roll
-- Enhances attack for party members within area of effect
-- Optimal Job: Dark Knight
-- Lucky Number: 4
-- Unlucky Number: 8
-- Level: 14
-- Phantom Roll +1 Value: 3
--
-- Die Roll    |No DRK    |With DRK
-- --------    -------- -----------
-- 1           |6%      |16%
-- 2           |8%      |18%
-- 3           |9%      |19%
-- 4           |25%     |35%
-- 5           |11%     |21%
-- 6           |13%     |23%
-- 7           |16%     |26%
-- 8           |3%      |13%
-- 9           |17%     |27%
-- 10          |19%     |29%
-- 11          |31%     |41%
-- Bust        |-10%    |-10%
-----------------------------------
require("scripts/globals/settings")
require("scripts/globals/ability")
require("scripts/globals/status")
require("scripts/globals/msg")
-----------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)
	local roll = math.random(1, 11)
	local power = {6, 8, 9, 25, 11, 13, 16, 3, 17, 19, 31}
	local potency = power[roll]
	
	if target:hasStatusEffect(tpz.effect.CHAOS_ROLL) then
		return
	else
	MobBuffMove(mob, tpz.effect.CHAOS_ROLL, potency, 0, 180)
	end
	
	skill:setMsg(tpz.msg.basic.ROLL_MAIN)
	
	return roll
end