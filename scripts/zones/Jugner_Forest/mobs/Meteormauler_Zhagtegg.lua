-----------------------------------
-- Area: Jugner Forest
--   NM: Meteormauler Zhagtegg
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    UpdateNMSpawnPoint(mob:getID())
    local respawn = (75600 + math.random(600, 900)) -- 21 hours, plus 10 to 15 min
    mob:setRespawnTime(respawn)
    SetServerVariable("ZhagteggRespawn",(os.time() + respawn))   
end