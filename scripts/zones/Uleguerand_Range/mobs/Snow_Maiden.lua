-----------------------------------
-- Area: Uleguerand Range
--  Mob: Snow Maiden
-----------------------------------
local ID = require("scripts/zones/Uleguerand_Range/IDs")
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    local phIndex = mob:getLocalVar("phIndex")
    local ph = GetMobByID(ID.mob.SNOW_MAIDEN_PH[phIndex])

    -- allow current placeholder to respawn
    DisallowRespawn(mob:getID(), true)
    DisallowRespawn(ph:getID(), false)
    ph:setRespawnTime(GetMobRespawnTime(ph:getID()))
end

function onMobSpawn(mob)
	mob:setMod(tpz.mod.REGEN, 25)
    mob:setLocalVar("timeToGrow", os.time() + math.random(86400, 108000)) -- 24 to 30 hours
end

function onMobDeath(mob)
    local phIndex = mob:getLocalVar("phIndex")
    local ph = GetMobByID(ID.mob.SNOW_MAIDEN_PH[phIndex])
    -- pick next placeholder
    phIndex = (phIndex % 3) + 1
    ph = GetMobByID(ID.mob.SNOW_MAIDEN_PH[phIndex])
    ph:setLocalVar("timeToGrow", os.time() + math.random(86400, 108000)) -- 24 to 30 hours
 	ph:setLocalVar("phIndex", phIndex)
end

function onMobDisengage(mob)
    mob:setLocalVar("timeToGrow", os.time() + math.random(86400, 108000)) -- 24 to 30 hours
end

function onMobRoam(mob)
    -- Snow Maiden has been left alone for 24 to 30 hours
    if os.time() >= mob:getLocalVar("timeToGrow") then
        DisallowRespawn(ID.mob.SNOW_MAIDEN, true)
        DespawnMob(ID.mob.SNOW_MAIDEN)
        DisallowRespawn(ID.mob.FATHER_FROST, false)
        pos = mob:getPos()
        SpawnMob(ID.mob.FATHER_FROST):setPos(pos.x, pos.y, pos.z, pos.rot)
    end
end