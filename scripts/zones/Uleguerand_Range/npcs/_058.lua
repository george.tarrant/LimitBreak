----------------------------------
-- Area: Ule Range
--  NPC: Water Fall
-----------------------------------
local ID = require("scripts/zones/Uleguerand_Range/IDs")
require("scripts/globals/npc_util")
-----------------------------------

--[[ makes door targetable again
	UPDATE npc_list SET flag = '0', entityFlags = '3' WHERE npcid = '16798112';
  ]]
function onTrade(player, npc, trade)
    if npcUtil.tradeHas(trade, 4104) then -- Fire Cluster Trade
        player:confirmTrade()
        GetNPCByID(npc:getID()):openDoor(60)
    end
end

function onTrigger(player, npc)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
