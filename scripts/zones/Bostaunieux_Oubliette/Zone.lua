-----------------------------------
--
-- Zone: Bostaunieux_Oubliette (167)
--
-----------------------------------
local ID = require("scripts/zones/Bostaunieux_Oubliette/IDs")
require("scripts/globals/conquest")
-----------------------------------

function onInitialize(zone)

    local drexerionre = GetServerVariable("DrexerionRespawn")
    UpdateNMSpawnPoint(ID.mob.DREXERION_THE_CONDEMNED)
    if os.time() < drexerionre then
        GetMobByID(ID.mob.DREXERION_THE_CONDEMNED):setRespawnTime(drexerionre - os.time())
    else
        GetMobByID(ID.mob.DREXERION_THE_CONDEMNED):setRespawnTime(300)
    end
	
    local phanduronre = GetServerVariable("DrexerionRespawn")
    UpdateNMSpawnPoint(ID.mob.PHANDURON_THE_CONDEMNED)
    if os.time() < phanduronre then
        GetMobByID(ID.mob.PHANDURON_THE_CONDEMNED):setRespawnTime(phanduronre - os.time())
    else
        GetMobByID(ID.mob.PHANDURON_THE_CONDEMNED):setRespawnTime(300)
    end
	
    local bsre = GetServerVariable("BloodSuckerRespawn")
    UpdateNMSpawnPoint(ID.mob.BLOODSUCKER)
    if os.time() < bsre then
        GetMobByID(ID.mob.BLOODSUCKER):setRespawnTime(bsre - os.time())
    else
        GetMobByID(ID.mob.BLOODSUCKER):setRespawnTime(300)
    end
end

function onZoneIn(player, prevZone)
    local cs = -1
    if player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0 then
        player:setPos(99.978, -25.647, 72.867, 61)
    end
    return cs
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onRegionEnter(player, region)
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
