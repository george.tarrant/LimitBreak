-----------------------------------
-- Area: Waughroon Shrine
-- Mob: Gaki
-- A Thief in Norg!? BCNM Fight
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobSpawn (mob)
    mob:setMobMod(tpz.mobMod.NO_STANDBACK, 1)
end