-----------------------------------
-- Area: Garlaige Citadel
-- Mob: Guardian Statue
-- Involved in Quest: Peace for the Spirit - RDM AF3
-----------------------------------
require("scripts/globals/status")
require("scripts/globals/mobs")
-----------------------------------

function onMobSpawn(mob)
    mob:setMod(tpz.mod.REGAIN, 150)
    mob:addMod(tpz.mod.DOUBLE_ATTACK, 10)
end

function onMobFight(mob)
    if mob:getTP() >= 1000 then
        mob:useMobAbility()
    end

    if mob:getHPP() <= 50 then
        mob:setMod(tpz.mod.REGAIN, 250)
    elseif mob:getHPP() <= 25 then
        mob:setMod(tpz.mod.REGAIN, 400)
    end
end