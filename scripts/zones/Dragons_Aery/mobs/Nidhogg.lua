-----------------------------------
-- Area: Dragons Aery
--  HNM: Nidhogg
-----------------------------------
local ID = require("scripts/zones/Dragons_Aery/IDs")
mixins = {require("scripts/mixins/rage")}
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/titles")
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.DRAW_IN, 1)
    mob:setMobMod(tpz.mobMod.DRAW_IN_CUSTOM_RANGE, 20)

    if LandKingSystem_NQ > 0 or LandKingSystem_HQ > 0 then
        GetNPCByID(ID.npc.FAFNIR_QM):setStatus(tpz.status.DISAPPEAR)
    end

    mob:addMod(tpz.mod.STUNRESTRAIT, 90)
    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
    mob:setLocalVar("[rage]timer", 3600) -- 60 minutes
    mob:setMobMod(tpz.mobMod.GIL_MIN, 28000)
    mob:setMobMod(tpz.mobMod.GIL_MAX, 32000)
end

function onMobFight(mob, target)
    local battletime = mob:getBattleTime()
    local twohourTime = mob:getLocalVar("twohourTime")

    if twohourTime == 0 then
        mob:setLocalVar("twohourTime", math.random(30, 90))
    end

    if battletime >= twohourTime then
        mob:useMobAbility(1053) -- Legitimately captured super_buff ID
        mob:setLocalVar("twohourTime", battletime + math.random(60, 120))
    end
end

function onMobDeath(mob, player, isKiller)
    player:addTitle(tpz.title.NIDHOGG_SLAYER)
end

function onMobDespawn(mob)
    local respawn = (75600 + ((math.random(0, 6)) * 1800)) -- 21 - 24 hours with half hour windows
    -- Disable Nidhogg Respawn on Death
    SetServerVariable("NidhoggUP", 0)
    DisallowRespawn(mob:getID(), true)

    -- Set Fafnir's spawnpoint and respawn time (21-24 hours)
    SetServerVariable("[PH]Nidhogg", 0)
    DisallowRespawn(ID.mob.FAFNIR, false)
    UpdateNMSpawnPoint(ID.mob.FAFNIR)
    GetMobByID(ID.mob.FAFNIR):setRespawnTime(respawn)
    SetServerVariable("FafnirRespawn",(os.time() + respawn))
end