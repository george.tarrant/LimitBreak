-----------------------------------
-- Area: Eastern Altepa Desert (114)
--   NM: Centurio XII-I
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobSpawn(mob)
    -- Takes only 50% damage from both physical and magical attacks. Enfeebles are at best half-resisted.
    mob:addMod(tpz.mod.DMGPHYS,-50)
    mob:addMod(tpz.mod.DMGMAGIC,-50)
    mob:addMod(tpz.mod.STATUSRES,50)
    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    UpdateNMSpawnPoint(mob:getID())
    local respawn = (75600 + math.random(600, 900)) -- 21 hours, plus 10 to 15 min
    mob:setRespawnTime(respawn)
    SetServerVariable("CenturioXII-IRespawn",(os.time() + respawn))
end
