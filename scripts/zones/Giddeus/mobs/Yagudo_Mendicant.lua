-----------------------------------
-- Area: Giddeus (145)
--  Mob: Yagudo Mendicant
-----------------------------------
local ID = require("scripts/zones/Giddeus/IDs")
require("scripts/globals/mobs")
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobDeath(mob)
    local HOOre = GetServerVariable("HOOrespawn")
    local count = GetServerVariable("HOOcount")
    if os.time() > HOOre then
        if
            (mob:getID() == 17371490) or
            (mob:getID() == 17371498) or
            (mob:getID() == 17371486) or
            (mob:getID() == 17371504) or
            (mob:getID() == 17371508) or
            (mob:getID() == 17371513) then

            count = count + 1
            if count >= 100 then
                tpz.mob.phOnDespawn(mob, ID.mob.HOO_MJUU_THE_TORRENT_PH, 5, 3600) -- 1 hour
            elseif count >= 75 then
                tpz.mob.phOnDespawn(mob, ID.mob.HOO_MJUU_THE_TORRENT_PH, 4, 3600) -- 1 hour
            elseif count >= 50 then
                tpz.mob.phOnDespawn(mob, ID.mob.HOO_MJUU_THE_TORRENT_PH, 3, 3600) -- 1 hour
            else
                tpz.mob.phOnDespawn(mob, ID.mob.HOO_MJUU_THE_TORRENT_PH, 2, 3600) -- 1 hour
            end
            SetServerVariable("HOOcount", count)
        end
     end
end
