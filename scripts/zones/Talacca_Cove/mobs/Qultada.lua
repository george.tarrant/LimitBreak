-----------------------------------
-- Area: Jade Sepulcher
--  Mob: Qultada
-- COR LB5
-----------------------------------
local ID = require("scripts/zones/Talacca_Cove/IDs")
require("scripts/globals/status")
require("scripts/globals/mobs")
-----------------------------------

	
function onMobInitialize(mob)

--	mob:addListener("WEAPONSKILL_STATE_ENTER", "WS_START_MSG", function(mob, wsid)
--		if (wsid ~= nil) then
--			mob:showText(mob, ID.text.RAUBAHN_WEAPON_SKILL)
--		end
--	end)
--	mob:addListener("MAGIC_START", "MAGIC_MSG", function(mob, spell, action)
--		
--		local message = math.random(1,2)
--			if message == 1 then
--			mob:showText(mob, ID.text.RAUBAHN_CASTING_ONE)
--			elseif message == 2 then
--			mob:showText(mob, ID.text.RAUBAHN_CASTING_TWO)
--			end
--		
--	end)
--	
end

function onMobSpawn(mob)
    mob:setLocalVar("Percent", 1)
	mob:setLovalCar("NextShot", 1)
end


function onMobFight(mob, player)
	local hpp = mob:getHPP()
	local percentchat = mob:getLocalVar("Percent")
	local bf = mob:getBattlefield()
	--local test = 1
	
	if percentchat == 1 and hpp <= 70 then
		mob:showText(mob, ID.text.QULTADA_HEAT)
		mob:setLocalVar("Percent", 2)	
	elseif percentchat == 2 and hpp <= 40 then
		mob:showText(mob, ID.text.QULTADA_HEAT)
		mob:setLocalVar("Percent", 3)
	end
	
	if hpp <= 20 then
		mob:showText(mob, ID.text.QULTADA_VICTORY)
		bf:setStatus(tpz.battlefield.status.WON)
		mob:setStatus(tpz.status.DISAPPEAR)
    end
	
	if (mob:getBattleTime()/20) == mob:getLocalVar("NextShot") then
		mob:setLocalVar("NextShot",(mob:getLocalVar("NextShot") + 1))
		if math.random(1,3) == 1 then -- 33% chance to use a shot every 20 seconds, avg 1 per min.
			local shotlist = {2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016}
			mob:useMobAbility(shotlist[math.random(#shotlist)])
		end
	end
end

function onMobEngaged(mob, target)
	local roll = math.random(1, 3)
	local phantom = {105, 108, 110}
	
	target:showText(mob, ID.text.QULTADA_ENGAGE)
	mob:useMobAbility(phantom[roll])
	target:showText(mob, ID.text.QULTADA_ROLL_1)
end

function onMobDeath(mob, player, isKiller)
	player:showText(mob, ID.text.QULTADA_VICTORY)
end
