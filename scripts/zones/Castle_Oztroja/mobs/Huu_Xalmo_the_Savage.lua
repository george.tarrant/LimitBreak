-----------------------------------
-- Area: Castle Oztroja (151)
--   NM: Huu Xalmo the Savage
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
   mob:setMod(tpz.mod.PARALYZERESTRAIT, 75)
   mob:setMod(tpz.mod.COUNTER, 30)
   mob:setMod(tpz.mod.ACC, 275)
   mob:setMod(tpz.mod.DEF, 300)
end

function onMobDeath(mob, player, isKiller)
end
