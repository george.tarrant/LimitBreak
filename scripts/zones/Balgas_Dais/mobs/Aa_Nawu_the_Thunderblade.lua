-----------------------------------
-- Area: Balga's Dais
--  Mob: Aa Nawu the Thunderblade - SAM
-- BCNM: Divine Punishers
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobSpawn(mob)
    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = 730, hpp = 60}, -- uses meikyo_shisui
        },
    })
end