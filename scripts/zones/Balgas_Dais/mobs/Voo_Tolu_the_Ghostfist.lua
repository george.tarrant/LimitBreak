-----------------------------------
-- Area: Balga's Dais
--  Mob: Voo Tolu the Ghostfist - MNK
-- BCNM: Divine Punishers
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobSpawn(mob)
    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = 690, hpp = 60}, -- use hundred fists at 60
        },
    })
end