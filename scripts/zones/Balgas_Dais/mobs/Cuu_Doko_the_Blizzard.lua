-----------------------------------
-- Area: Balga's Dais
--  Mob: Cuu Doko the Blizzard - WHM
-- BCNM: Divine Punishers
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobSpawn(mob)
    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = 689, hpp = math.random(10,25)}, -- uses Benediction between 10 and 25%
        },
    })
end