-----------------------------------
-- Area: Balga's Dais
--  Mob: Yoo Mihi the Haze - NIN
-- BCNM: Divine Punishers
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobSpawn(mob)
    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = 731, hpp = 50}, -- uses mijin_gakure at 50
        },
    })
end