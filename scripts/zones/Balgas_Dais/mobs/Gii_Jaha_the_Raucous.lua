-----------------------------------
-- Area: Balga's Dais
--  Mob: Gii Jaha the Raucous - BRD
-- BCNM: Divine Punishers
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobSpawn(mob)
    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = 696, hpp = math.random(45,80)}, -- uses soul voice
        },
    })
end