-----------------------------------
-- Area: Ghelsba Outpost
--  Mob: Kilioa
-- BCNM: Petrifying Pair
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.NO_MOVE, 1)
    mob:setMobMod(tpz.mobMod.LINK_RADIUS, 30)
    mob:setMobMod(tpz.mobMod.SOUND_RANGE, 20)
end

function onMobEngaged(mob, target)
    mob:setMobMod(tpz.mobMod.NO_MOVE, 0)
    mob:useMobAbility(373) -- Secretion
end

function onMobWeaponSkillPrepare(mob, target)
    local rnd = math.random() -- Per wiki, mobs in Petrifying Pair heavily favor Baleful Gaze.

    if rnd > 0.4 then
    return 370
    end
end


function onMobDeath(mob, player, isKiller)
end
