-----------------------------------
-- Area: Port Windurst
--  NPC: Pankii-Mankii
-- Working 100%
-----------------------------------
require("scripts/globals/settings")
local ID = require("scripts/zones/Port_Windurst/IDs")
-----------------------------------

function onTrade(player, npc, trade)
end

--[[function onTrigger(player, npc)
    player:startEvent(212)
end]]--

function onTrigger(player, npc)
    local LvL = player:getMainLvl()

    if player:getCharVar("AnnyRing") == 0 and (player:getFreeSlotsCount() >= 1) then
        player:addItem(15793)
        player:messageSpecial(ID.text.ITEM_OBTAINED, 15793)
        player:setCharVar("AnnyRing", 1)
        player:PrintToPlayer("Welcome to Limitbreak!",29)
    elseif  player:getCharVar("AnnyRing") == 0 and (player:getFreeSlotsCount() == 0) then
        player:messageSpecial(ID.text.ITEM_CANNOT_BE_OBTAINED, 15793)
    elseif player:getCharVar("AnnyRing") == 1 and LvL >= 30 and (player:getFreeSlotsCount() >= 1) then
        if player:hasItem(15793) then
            player:PrintToPlayer("You already have one, come back when it's gone!",29)
        else
            player:addItem(15793)
            player:messageSpecial(ID.text.ITEM_OBTAINED, 15793)
            player:setCharVar("AnnyRing", 2)
            player:PrintToPlayer("Congratulations on level 30, here is your reward!",29)
        end
    elseif  (player:getCharVar("AnnyRing") == 1) and LvL >= 30 and (player:getFreeSlotsCount() == 0) then
        player:messageSpecial(ID.text.ITEM_CANNOT_BE_OBTAINED, 15793)
    else
        player:PrintToPlayer("You are either not level 30 or you have received two rings already.",29)
    end
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
