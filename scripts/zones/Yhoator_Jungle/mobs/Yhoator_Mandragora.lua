-----------------------------------
-- Area: Yhoator Jungle
--  Mob: Yhoator Mandragora
-----------------------------------
require("scripts/globals/regimes")
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
    mob:addMod(tpz.mod.ATTP, -50)
end

function onMobDeath(mob, player, isKiller)
    tpz.regime.checkRegime(player, mob, 130, 1, tpz.regime.type.FIELDS)
end
