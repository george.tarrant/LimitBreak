-----------------------------------
--
-- Zone: Yuhtunga_Jungle (123)
--
-----------------------------------
local ID = require("scripts/zones/Yuhtunga_Jungle/IDs")
require("scripts/quests/i_can_hear_a_rainbow")
require("scripts/globals/chocobo_digging")
require("scripts/globals/conquest")
require("scripts/globals/helm")
require("scripts/globals/zone")
require("scripts/globals/beastmentreasure")
-----------------------------------

function onChocoboDig(player, precheck)
    return tpz.chocoboDig.start(player, precheck)
end

function onInitialize(zone)

    local mewwre = GetServerVariable("MewwRespawn")
--  UpdateNMSpawnPoint(ID.mob.MEWW_THE_TURTLERIDER)
    if os.time() < mewwre then
        GetMobByID(ID.mob.MEWW_THE_TURTLERIDER):setRespawnTime(mewwre - os.time())
    else
        GetMobByID(ID.mob.MEWW_THE_TURTLERIDER):setRespawnTime(300)
    end

    local roseup = GetServerVariable("RoseGardenUP")
    local vivianup = GetServerVariable("VivianUP")
    if RoseGardenUP == 1 then
        DisallowRespawn(ID.mob.ROSE_GARDEN_PH, true)
        DespawnMob(ID.mob.ROSE_GARDEN_PH)
        DisallowRespawn(ID.mob.ROSE_GARDEN, false)
        pos = mob:getPos()
        SpawnMob(ID.mob.ROSE_GARDEN):setPos(pos.x, pos.y, pos.z, pos.rot)
    elseif vivianUP == 1 then
        DisallowRespawn(ID.mob.ROSE_GARDEN_PH, true)
        DespawnMob(ID.mob.ROSE_GARDEN_PH)
        DisallowRespawn(ID.mob.ROSE_GARDEN, true)
        DespawnMob(ID.mob.ROSE_GARDEN)
        DisallowRespawn(ID.mob.VOLUPTUOUS_VILMA, false)
        pos = mob:getPos()
        SpawnMob(ID.mob.VOLUPTUOUS_VILMA):setPos(pos.x, pos.y, pos.z, pos.rot)
    end

    tpz.conq.setRegionalConquestOverseers(zone:getRegionID())

    tpz.helm.initZone(zone, tpz.helm.type.HARVESTING)
    tpz.helm.initZone(zone, tpz.helm.type.LOGGING)

    tpz.bmt.updatePeddlestox(tpz.zone.YUHTUNGA_JUNGLE, ID.npc.PEDDLESTOX)
end

function onGameDay()
    tpz.bmt.updatePeddlestox(tpz.zone.YUHTUNGA_JUNGLE, ID.npc.PEDDLESTOX)
	SetServerVariable("[DIG]ZONE123_ITEMS", 0)
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onZoneIn( player, prevZone)
    local cs = -1

    if player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0 then
        player:setPos(116.825, 6.613, 100, 140)
    end

    if quests.rainbow.onZoneIn(player) then
        cs = 11
    end

    return cs
end

function onRegionEnter( player, region)
end

function onEventUpdate( player, csid, option)
    if csid == 11 then
        quests.rainbow.onEventUpdate(player)
    end
end

function onEventFinish( player, csid, option)
end
