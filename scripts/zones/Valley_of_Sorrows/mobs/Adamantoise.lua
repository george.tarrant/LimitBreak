-----------------------------------
-- Area: Valley of Sorrows
--  HNM: Adamantoise
-----------------------------------
local ID = require("scripts/zones/Valley_of_Sorrows/IDs")
mixins = {require("scripts/mixins/rage")}
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/titles")
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.DRAW_IN, 1)
    mob:setMobMod(tpz.mobMod.DRAW_IN_CUSTOM_RANGE, 20)

    if LandKingSystem_NQ > 0 or LandKingSystem_HQ > 0 then
        GetNPCByID(ID.npc.ADAMANTOISE_QM):setStatus(tpz.status.DISAPPEAR)
    end
    if LandKingSystem_HQ == 0 then
        SetDropRate(21, 3344, 0) -- do not drop clump_of_red_pondweed
    end

    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
    mob:setLocalVar("[rage]timer", 1800) -- 30 minutes
    mob:setMobMod(tpz.mobMod.GIL_MIN, 15000)
    mob:setMobMod(tpz.mobMod.GIL_MAX, 15000)
end

function onMobDeath(mob, player, isKiller)
    player:addTitle(tpz.title.TORTOISE_TORTURER)
end

function onMobDespawn(mob)
    local kills = GetServerVariable("[PH]Aspidochelone") + 1
    local respawn = (75600 + ((math.random(0, 6)) * 1800)) -- 21 - 24 hours with half hour windows
    local spawnChance = 0
    print(kills)
    print(spawnChance)
    if kills > 2 then
        -- 4th Day = 15%
        -- 5th Day = 27.5%
        -- 6th Day = 40%
        -- 7th Day = 52.5%
        -- 8th Day = 65%
        -- 9th Day = 77.5%
        -- 10th Day = 90%
        -- 11th Day = 100%
        spawnChance = 150 + ((kills - 3) * 125)
        print(spawnChance)
    end
    local aspid = math.random(1000)
    print(aspid)
    if kills > 2 and (aspid <= spawnChance) then
        -- ASPIDOCHELONE
        print("aspid pop")
        SetServerVariable("AspidocheloneUP", 1)
        DisallowRespawn(ID.mob.ASPIDOCHELONE, false);
        DisallowRespawn(ID.mob.ADAMANTOISE, true);
        UpdateNMSpawnPoint(ID.mob.ASPIDOCHELONE);
        GetMobByID(ID.mob.ASPIDOCHELONE):setRespawnTime(respawn)
        SetServerVariable("AspidocheloneRespawn",(os.time() + respawn))
    else
        -- ADAMANTOISE
        print("adam pop")
        DisallowRespawn(ID.mob.ADAMANTOISE, false)
        DisallowRespawn(ID.mob.ASPIDOCHELONE, true)
        UpdateNMSpawnPoint(ID.mob.ADAMANTOISE);
        GetMobByID(ID.mob.ADAMANTOISE):setRespawnTime(respawn)
        SetServerVariable("[PH]Aspidochelone", kills)
        SetServerVariable("AdamantoiseRespawn",(os.time() + respawn))
    end
end