-----------------------------------
-- Area: North Gustaberg
--  Mob: Gambilox Wanderling
-- Quest NM - "As Thick as Thieves"
-----------------------------------
require("scripts/globals/status")
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.IDLE_DESPAWN, 300)
	mob:setMod(tpz.mod.REGAIN, 50)
end

function onMobDeath(mob, player, isKiller)
    if player:getCharVar("thickAsThievesGamblingCS") == 5 then
        player:setCharVar("thickAsThievesGamblingCS", 6)
    end
end
