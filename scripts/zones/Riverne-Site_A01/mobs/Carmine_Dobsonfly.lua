-----------------------------------
-- Area: Riverne - Site A01
--  Mob: Carmine Dobsonfly
-----------------------------------
local ID = require("scripts/zones/Riverne-Site_A01/IDs")
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.SUPERLINK, ID.mob.CARMINE_DOBSONFLY_OFFSET)
    mob:SetMagicCastingEnabled(false) -- does not cast spells while idle
    SetServerVariable("allFliesDead", 1)
end

function onMobEngaged(mob, target)
    mob:SetMagicCastingEnabled(true)
end

function onMobDisengage(mob)
    mob:SetMagicCastingEnabled(false)
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    -- each dead dobsonfly should stay despawned until all 10 are killed. then they respawn as a group.


    local flyBase = ID.mob.CARMINE_DOBSONFLY_OFFSET -- ID for first fly
    DisallowRespawn(mob:getID(), true) -- will prevent mob from respawning after despawn, unless check below passes and re-enables
    
    if 
        (GetMobByID(flyBase):isAlive()) or
        (GetMobByID(flyBase+1):isAlive()) or
        (GetMobByID(flyBase+2):isAlive()) or
        (GetMobByID(flyBase+3):isAlive()) or
        (GetMobByID(flyBase+4):isAlive()) or
        (GetMobByID(flyBase+5):isAlive()) or
        (GetMobByID(flyBase+6):isAlive()) or
        (GetMobByID(flyBase+7):isAlive()) or
        (GetMobByID(flyBase+8):isAlive()) or
        (GetMobByID(flyBase+9):isAlive()) then

            SetServerVariable("allFliesDead", 0) -- clear this variable if any of the above 10 flies are alive confriming not all flies are dead.
    else 
        local respawnTime = math.random(75600, 86400) -- 21 to 24 hours
        for i = flyBase, flyBase + 9 do -- individually enables respawn of each fly and assigns correct respawn time now that we know all 10 are dead.
            DisallowRespawn(i, false)
            GetMobByID(i):setRespawnTime(respawnTime)
        end
        SetServerVariable("DobsonflyRespawn",(os.time() + respawnTime))
        SetServerVariable("allFliesDead", 1) -- set variable indicating all flies have been killed.
    end
end
