-----------------------------------
-- Area: Riverne - Site A01
--  Mob: Cloud Hippogryph
-- Note:
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
end

function onAdditionalEffect(mob, target, damage)
    local rand = math.random(1,2)
    if rand == 1 then
        return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.WEIGHT)
    end
end

function onMobDeath(mob, player, isKiller)
end
