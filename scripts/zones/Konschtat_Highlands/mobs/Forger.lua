-----------------------------------
-- Area: Konschtat Highlands
--   NM: Forger
-----------------------------------
local ID = require("scripts/zones/Konschtat_Highlands/IDs")
require("scripts/globals/status")
require("scripts/quests/tutorial")
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.IDLE_DESPAWN, 300)
end

function onMobSpawn(mob)
    mob:setMod(tpz.mod.DOUBLE_ATTACK, 50)
	mob:setMod(tpz.mod.ATTP, 100)
end	
function onMobDeath(mob, player, isKiller)
    tpz.tutorial.onMobDeath(player)
	if (isKiller and GetMobByID(ID.mob.FORGER):isDead()) then
    GetNPCByID(ID.npc.QM2):setLocalVar("cooldown", os.time() + 60)
	end
end
