-----------------------------------
-- Area: Nashmau
--  NPC: Yoyoroon
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Nashmau/IDs")
require("scripts/globals/shop")

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    local stock =
    {
        2239,  4050,    -- Tension Spring
        2243,  4050,    -- Loudspeaker
        2246,  4050,    -- Accelerator
        2251,  4050,    -- Armor Plate
        2254,  4050,    -- Stabilizer
        2258,  4050,    -- Mana Jammer
        2262,  4050,    -- Auto-Repair Kit
        2266,  4050,    -- Mana Tank
        2240,  8100,    -- Inhibitor
        --9229,  9925,    -- Speedloader
        2242,  8100,    -- Mana Booster
        2247,  8100,    -- Scope
        2250,  8100,    -- Shock Absorber
        2255,  8100,    -- Volt Gun
        2260,  8100,    -- Stealth Screen
        2264,  8100,    -- Damage Gauge
        2268,  8100,    -- Mana Conserver
        --2238, 19890,    -- Strobe
        --2409, 19890,    -- Flame Holder
        --2410, 19890,    -- Ice Maker
        --2248, 19890,    -- Pattern Reader
        --2411, 19890,    -- Replicator
        --2252, 19890,    -- Analyzer
        --2256, 19890,    -- Heat Seeker
        --2259, 19890,    -- Heatsink
        --2263, 19890,    -- Flashbulb
        --2267, 19890,    -- Mana Converter
    }

    player:showText(npc, ID.text.YOYOROON_SHOP_DIALOG)
    tpz.shop.general(player, stock, JEUNO)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
