-----------------------------------
-- Area: Davoi
--  Mob: Orcish Beastrider
-- Note: PH for Poisonhand Gnadgad
-----------------------------------
local ID = require("scripts/zones/Davoi/IDs")
require("scripts/globals/mobs")
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobDeath(mob)
    local GNADre = GetServerVariable("GNADrespawn")
    local count = GetServerVariable("GNADcount")
    if os.time() > GNADre then
        if
            (mob:getID() == 17387635) or
            (mob:getID() == 17387639) then

            count = count + 1
            if count >= 55 then
                tpz.mob.phOnDespawn(mob, ID.mob.POISONHAND_GNADGAD_PH, 5, 3600) -- 1 hour
            elseif count >= 40 then
                tpz.mob.phOnDespawn(mob, ID.mob.POISONHAND_GNADGAD_PH, 4, 3600) -- 1 hour
            elseif count >= 25 then
                tpz.mob.phOnDespawn(mob, ID.mob.POISONHAND_GNADGAD_PH, 3, 3600) -- 1 hour
            else
                tpz.mob.phOnDespawn(mob, ID.mob.POISONHAND_GNADGAD_PH, 2, 3600) -- 1 hour
            end
            SetServerVariable("GNADcount", count)
        end
     end
end
