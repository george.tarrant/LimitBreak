-----------------------------------
-- Area: Davoi
--  Mob: Deloknok
-- Involved in Quest: The First Meeting
-----------------------------------
require("scripts/globals/keyitems")
require("scripts/globals/quests")
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
    local theFirstMeeting = player:getQuestStatus(BASTOK, tpz.quest.id.bastok.THE_FIRST_MEETING)
    local martialArtsScroll = player:hasKeyItem(tpz.ki.SANDORIAN_MARTIAL_ARTS_SCROLL)

    if (theFirstMeeting == QUEST_ACCEPTED and martialArtsScroll == false and player:getCharVar("theFirstMeetingMyMob") == 1) then
        player:addCharVar("theFirstMeetingKilledNM", 1)
    end

end
