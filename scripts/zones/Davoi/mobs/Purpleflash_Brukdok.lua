-----------------------------------
-- Area: Davoi
-- Mob: Purpleflash Brukdok
-- RDM AF Weapon
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.MAGIC_COOL, 40)
end
