-----------------------------------
-- Area: Bastok Markets
--  NPC: Ciqala
-- Type: Merchant
-- !pos -283.147 -11.319 -143.680 235
-----------------------------------
local ID = require("scripts/zones/Bastok_Markets/IDs")
require("scripts/globals/shop")

function onTrigger(player, npc)
    local stock =
    {
        -- Only available while Bastok is in first.
        16392, 4818, 1,  -- Metal Knuckles
        16643, 11285, 1, -- Battleaxe
        16705, 4186, 1,  -- Greataxe
        17044, 6033, 1,  -- Warhammer
        -- Available when Bastok is in first or second.
        16391, 828, 2,   -- Brass Knuckles
        16641, 1435, 2,  -- Brass Axe
        16704, 618, 2,   -- Butterfly Axe
        17043, 2129, 2,  -- Brass Hammer
        -- Always available.
        16390, 224, 3,   -- Bronze Knuckles
        16640, 290, 3,   -- Bronze Axe
        17042, 312, 3,   -- Bronze Hammer
        17049, 47, 3,    -- Maple Wand
        17088, 58, 3,    -- Ash Staff
    }
    player:showText(npc, ID.text.CIQALA_SHOP_DIALOG)
    tpz.shop.nation(player, stock, tpz.nation.BASTOK)
end
