-----------------------------------
-- Area: Ru'Aun Gardens
--   NM: Byakko 17309982
-- Spawns With: 
-- HP: 21000
-- EVA: 473
-- DEF: 440
-- ATK: 422
-- ACC: 388 + 42
-----------------------------------
local ID = require("scripts/zones/RuAun_Gardens/IDs")
mixins = {require("scripts/mixins/job_special")}
require("scripts/globals/mobs")
require("scripts/globals/status")
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
end

function onMobSpawn(mob)
	mob:setMod(tpz.mod.TRIPLE_ATTACK, 40)
	mob:addMod(tpz.mod.ACC, 42)
	mob:addMod(tpz.mod.DEF, 80)
end

function onAdditionalEffect(mob, target, damage)
    return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.ENLIGHT)
end

function onMobDeath(mob, player, isKiller)
    player:showText(mob, ID.text.SKY_GOD_OFFSET + 12)
end
