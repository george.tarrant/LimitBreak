The following information is currently copy-pasted from Project Wings, however we reserve the right to change it at will.

# LimitBreak Project

### License Notice
**Original contributions to the LimitBreak project (i.e. not originating from the Darkstar project or Project Topaz) are governed by the GNU AGPLv3 (rather than GPL), which requires modified versions to be shared back, unless marked as dual-licensed by their respective authors. See the [LICENSE](LICENSE) file for more details.**

**The new login server (Copyright (c) 2021 Wings Project) is not based on Topaz and is entirely under AGPLv3.

## Welcome to LimitBreak Project
The LimitBreak project is a server emulator for FFXI, aiming to reproduce Treasures of Aht Urhgan game experience as it was in August 2007.

### Website
https://www.limitbreakffxi.com/

### Forums
https://forum.limitbreakffxi.com/

### Discord
https://discord.gg/Rcj8tGzXwm


### Bug Reports
Bugs and issues pertaining to this code base should go to this project's bug tracker, which you can find here: https://gitlab.com/limitbreak/LimitBreak/-/issues

Before reporting issues please make sure that:
1. Your local tree is fully updated and that all server processes have been rebuilt form the latest code.
2. The database is in full sync with the latest source tree.
3. You are using the latest LimitBreak installer available in our [Discord](https://discord.gg/Rcj8tGzXwm).

## How to report bugs

1. Give a short description for the issue name (e.g: "Stone spell cost is wrong")
2. Provide all the steps to reproduce the problem (provide as many details as possible)
3. Include any additional information or reference links you might consider necessary

All of this helps speeding up the bugfix process!


### Pull Requests
**By submitting a Pull Request to the LimitBreak project, you agree to our [Limited Contributor License Agreement](CONTRIBUTOR_AGREEMENT.md)**

Commits should contain a descriptive name for what you are modifying

Check the contributing guide for the project style guide: [Contributing](CONTRIBUTING.md)

You must *test your code* before committing changes/submitting a pull request. You break it you fix it!

Submit early and often! Try not to touch the PR too much after its submission (unless we request changes).

Remember to check back for any feedback, and drop a comment once requested changes have been made (if there are any).
