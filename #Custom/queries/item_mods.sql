-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

INSERT IGNORE INTO item_mods VALUES(16506,431,1); -- Stun Kukri
INSERT IGNORE INTO item_mods VALUES(17614,431,1); -- Stun Kukri +1
INSERT IGNORE INTO item_mods VALUES(16432,431,1); -- Stun Jamadhars
INSERT IGNORE INTO item_mods VALUES(17484,431,1); -- Stun Jamadhars +1
INSERT IGNORE INTO item_mods VALUES(16503,431,1); -- Stun Knife
INSERT IGNORE INTO item_mods VALUES(17600,431,1); -- Stun Knife +1