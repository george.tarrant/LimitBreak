-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- -----------
-- GENERAL  --
-- -----------

-- Misc
	UPDATE npc_list SET flag = "0", entityFlags = "3" WHERE npcid = "16798112"; -- Update waterfull NPC to be targetable.
	UPDATE mob_groups SET HP = "50000" WHERE name = "tzee_xicu_the_manifest" and groupid = "39"; -- Set tzee xicu HP for non-wings shadowreign version
	UPDATE mob_droplist SET itemrate = "750", groupID = "1", groupRate ="1000" WHERE itemID = "17619" AND dropID  = "2511"; -- create group for dagger/staff 75/25
	UPDATE mob_droplist SET itemrate = "250", groupID = "1", groupRate ="1000" WHERE itemID = "17528" AND dropID  = "2511";	-- create group for dagger/staff 75/25
