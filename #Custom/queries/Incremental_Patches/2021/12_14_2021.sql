-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- -----------
-- GENERAL  --
-- -----------

--Crafting
UPDATE synth_recipes SET gold = "11" WHERE ID ="2005"; -- updates wool slop SYNTH GoldSmithing subcraft level, updated in base SQL querry
UPDATE synth_recipes SET gold = "11" WHERE ID ="7016"; -- updates wool slop DESYNTH GoldSmithing subcraft level, updated in base SQL querry