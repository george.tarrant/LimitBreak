-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

-- Mount Zhayolm
UPDATE mob_spawn_points SET groupid = 56 WHERE mobname = "Wamoura" AND mobid = 17027421;  -- Adding missing Wamoura
UPDATE mob_spawn_points SET groupid = 56 WHERE mobname = "Wamoura" AND mobid = 17027422;  -- Adding missing Wamoura
INSERT IGNORE INTO `mob_groups` VALUES(56, 4280, 61, 'Wamoura', 300, 0, 2608, 0, 0, 80, 82, 0);  -- Adding missing Wamoura

-- Bright-handed_Kunberry
UPDATE mob_pools SET skill_list_id = 244 WHERE name = "Bright-handed_Kunberry"; -- Bright-handed_Kunberry should not use Everyone's Rancor

-- ENM Brothers
DELETE from mob_skill_lists WHERE skill_list_name = "Eldertaur" AND mob_skill_id = "1359"; -- Eldertaur should not use Chthonian Ray
DELETE from mob_skill_lists WHERE skill_list_name = "Eldertaur" AND mob_skill_id = "1360"; -- Have Brothers ENM only use Ray moves when scripted
DELETE from mob_spawn_points WHERE mobid = "16801841"; -- Adjusting ENM Brothers spawn positions
DELETE from mob_spawn_points WHERE mobid = "16801842"; -- Adjusting ENM Brothers spawn positions
DELETE from mob_spawn_points WHERE mobid = "16801844"; -- Adjusting ENM Brothers spawn positions
DELETE from mob_spawn_points WHERE mobid = "16801845"; -- Adjusting ENM Brothers spawn positions
DELETE from mob_spawn_points WHERE mobid = "16801847"; -- Adjusting ENM Brothers spawn positions
DELETE from mob_spawn_points WHERE mobid = "16801848"; -- Adjusting ENM Brothers spawn positions
INSERT IGNORE INTO `mob_spawn_points` VALUES(16801841, 'Eldertaur', 'Eldertaur', 5,  -464.000, 0.101, 902.000, 64);
INSERT IGNORE INTO `mob_spawn_points` VALUES(16801842, 'Mindertaur', 'Mindertaur', 6, -456.00, 0.453, 902.00, 64);
INSERT IGNORE INTO `mob_spawn_points` VALUES(16801844, 'Eldertaur', 'Eldertaur', 5, -224.00, 0.5772, 501.900, 64);
INSERT IGNORE INTO `mob_spawn_points` VALUES(16801845, 'Mindertaur', 'Mindertaur', 6, -216.00, 0.5772, 501.900, 64);
INSERT IGNORE INTO `mob_spawn_points` VALUES(16801847, 'Eldertaur', 'Eldertaur', 5, -624.000, 0.566, 221.23, 64);
INSERT IGNORE INTO `mob_spawn_points` VALUES(16801848, 'Mindertaur', 'Mindertaur', 6, -616.000, 0.566, 221.23, 64);

-- NM Gyre-Carlin added
UPDATE mob_groups SET minLevel = 50, maxLevel = 50 WHERE groupid = 56 AND name = "Gyre-Carlin";
UPDATE mob_spawn_points SET pos_x = 26.045, pos_y = 0.247, pos_z = 169.315 WHERE mobid = 16814331 AND mobname = "Gyre-Carlin";

-- Uragnite family UPDATE
DELETE FROM mob_skill_lists WHERE skill_list_name = "Uragnite" AND skill_list_id = 251 AND mob_skill_id = 1572;
INSERT IGNORE INTO `mob_skill_lists` VALUES('UragniteShell', 1170, 1572);


-- Update ship npc speeds to their correct values
UPDATE npc_list SET name = 0x10, speed = 50, speedsub = 50 WHERE npcid  = 16785747;
UPDATE npc_list SET speed = 50 WHERE npcid  = 16785755;
UPDATE npc_list SET speed = 50, speedsub = 50 WHERE npcid  = 16785756;
UPDATE npc_list SET speed = 50, speedsub = 50 WHERE npcid  = 16785757;
UPDATE npc_list SET name = 0x11, speed = 50, speedsub = 50 WHERE npcid  = 16793913;
UPDATE npc_list SET name = 0x14, speed = 50, speedsub = 50 WHERE npcid  = 16982046;
UPDATE npc_list SET name = 0x14, speed = 50, speedsub = 50 WHERE npcid  = 16994327;
UPDATE npc_list SET name = 0x7 WHERE npcid  = 16994327;
INSERT IGNORE INTO `npc_list` VALUES(17207823, 0x0B, '', 0, 0.000, 200.000, 0.000, 1, 50, 50, 19, 0, 0, 0, 3, 0x0400000000000000000000000000000000000000, 0, NULL, 1);
UPDATE npc_list SET name = 0x8 WHERE npcid  = 17216176;
INSERT IGNORE INTO `npc_list` VALUES(17228354, 0x0C, '', 0, 0.000, 200.000, 0.000, 1, 50, 50, 19, 0, 0, 0, 3, 0x0400000000000000000000000000000000000000, 0, NULL, 1);
INSERT IGNORE INTO `npc_list` VALUES(17248814, 0x09, '', 0, 0.000, 200.000, 0.000, 1, 50, 50, 19, 0, 0, 0, 3, 0x0400000000000000000000000000000000000000, 0, NULL, 1);
UPDATE npc_list SET name = 0x0A WHERE npcid  = 17269223;
UPDATE npc_list SET name = 0x02, speed = 50, speedsub = 50 WHERE npcid  = 17727598;
UPDATE npc_list SET name = 0x00, speed = 50, speedsub = 50 WHERE npcid  = 17743971;
UPDATE npc_list SET name = 0x01 WHERE npcid  = 17760420;
UPDATE npc_list SET name = 0x06, speed = 50, speedsub = 50 WHERE npcid  = 17784936;
UPDATE npc_list SET name = 0x04, speed = 50, speedsub = 50 WHERE npcid  = 17793088;
UPDATE npc_list SET name = 0x03, speed = 50, speedsub = 50 WHERE npcid  = 17797182;
UPDATE npc_list SET name = 0x05, speed = 50, speedsub = 50 WHERE npcid  = 17801320;

-- Enmity updates
UPDATE abilities SET CE = 1 WHERE name = "shield_bash" AND abilityId = 46;
UPDATE abilities SET VE = 1800 WHERE name = "sentinel" AND abilityId = 48;
UPDATE abilities SET CE = 1 WHERE name = "divine_seal" AND abilityId = 74;
UPDATE abilities SET CE = 1 WHERE name = "elemental_seal" AND abilityId = 75;
UPDATE abilities SET VE = 0 WHERE name = "cover" AND abilityId = 79;
UPDATE abilities SET CE = 1, VE = 300 WHERE name = "rampart" AND abilityId = 92;
UPDATE abilities SET CE = 1 WHERE name = "light_shot" AND abilityId = 131;
UPDATE abilities SET CE = 1 WHERE name = "dark_shot" AND abilityId = 132;
UPDATE abilities SET CE = 1 WHERE name = "fealty" AND abilityId = 157;
UPDATE abilities SET CE = 1 WHERE name = "chivalry" AND abilityId = 158;
UPDATE spell_list SET VE = 300 WHERE name = "poisona" and spellid = 14;
UPDATE spell_list SET VE = 300 WHERE name = "paralyna" and spellid = 15;
UPDATE spell_list SET VE = 300 WHERE name = "blindna" and spellid = 16;
UPDATE spell_list SET VE = 300 WHERE name = "silena" and spellid = 17;
UPDATE spell_list SET VE = 300 WHERE name = "stona" and spellid = 18;
UPDATE spell_list SET VE = 300 WHERE name = "viruna" and spellid = 19;
UPDATE spell_list SET VE = 300 WHERE name = "cursna" and spellid = 20;
UPDATE spell_list SET CE = 1, VE = 300 WHERE name = "blink" and spellid = 53;
UPDATE spell_list SET CE = 1, VE = 300 WHERE name = "stoneskin" and spellid = 54;
UPDATE spell_list SET CE = 1, VE = 300 WHERE name = "aquaveil" and spellid = 54;
UPDATE spell_list SET VE = 320 WHERE name = "slow" and spellid = 56;
UPDATE spell_list SET VE = 300 WHERE name = "haste" and spellid = 57;
UPDATE spell_list SET VE = 320 WHERE name = "paralyze" and spellid = 58;
UPDATE spell_list SET VE = 320 WHERE name = "silence" and spellid = 59;
UPDATE spell_list SET VE = 300 WHERE name = "barfire" and spellid = 60;
UPDATE spell_list SET VE = 300 WHERE name = "barblizzard" and spellid = 61;
UPDATE spell_list SET VE = 300 WHERE name = "baraero" and spellid = 62;
UPDATE spell_list SET VE = 300 WHERE name = "barstone" and spellid = 63;
UPDATE spell_list SET VE = 300 WHERE name = "barthunder" and spellid = 64;
UPDATE spell_list SET VE = 300 WHERE name = "barwater" and spellid = 65;
UPDATE spell_list SET VE = 300 WHERE name = "barsleep" and spellid = 72;
UPDATE spell_list SET VE = 300 WHERE name = "barpoison" and spellid = 73;
UPDATE spell_list SET VE = 300 WHERE name = "barparalyze" and spellid = 74;
UPDATE spell_list SET VE = 300 WHERE name = "barblind" and spellid = 75;
UPDATE spell_list SET VE = 300 WHERE name = "barsilence" and spellid = 76;
UPDATE spell_list SET VE = 300 WHERE name = "barpetrify" and spellid = 77;
UPDATE spell_list SET VE = 300 WHERE name = "barvirus" and spellid = 78;
UPDATE spell_list SET VE = 320 WHERE name = "slow_ii" and spellid = 79;
UPDATE spell_list SET VE = 320 WHERE name = "paralyze_ii" and spellid = 80;
UPDATE spell_list SET VE = 300 WHERE name = "sandstorm" and spellid = 99;
UPDATE spell_list SET VE = 300 WHERE name = "enfire" and spellid = 100;
UPDATE spell_list SET VE = 300 WHERE name = "enblizzard" and spellid = 101;
UPDATE spell_list SET VE = 300 WHERE name = "enaero" and spellid = 102;
UPDATE spell_list SET VE = 300 WHERE name = "enstone" and spellid = 103;
UPDATE spell_list SET VE = 300 WHERE name = "enthunder" and spellid = 104;
UPDATE spell_list SET VE = 300 WHERE name = "enwater" and spellid = 105;
UPDATE spell_list SET VE = 300 WHERE name = "phalanx" and spellid = 106;
UPDATE spell_list SET VE = 300 WHERE name = "phalanx_ii" and spellid = 107;
UPDATE spell_list SET VE = 300 WHERE name = "regen" and spellid = 108;
UPDATE spell_list SET VE = 300 WHERE name = "refresh" and spellid = 109;
UPDATE spell_list SET VE = 300 WHERE name = "regen_ii" and spellid = 110;
UPDATE spell_list SET VE = 300 WHERE name = "regen_iii" and spellid = 111;
UPDATE spell_list SET VE = 480 WHERE name = "erase" and spellid = 143;
UPDATE spell_list SET CE = 1, VE = 80 WHERE name = "gravity" and spellid = 216;
UPDATE spell_list SET VE = 320 WHERE name = "poison" and spellid = 220;
UPDATE spell_list SET VE = 320 WHERE name = "poison_ii" and spellid = 221;
UPDATE spell_list SET VE = 320 WHERE name = "poisonga" and spellid = 225;
UPDATE spell_list SET VE = 320 WHERE name = "bio" and spellid = 230;
UPDATE spell_list SET VE = 320 WHERE name = "bio_ii" and spellid = 231;
UPDATE spell_list SET VE = 320 WHERE name = "bio_iii" and spellid = 232;
UPDATE spell_list SET VE = 300 WHERE name = "burn" and spellid = 235;
UPDATE spell_list SET VE = 300 WHERE name = "frost" and spellid = 236;
UPDATE spell_list SET VE = 300 WHERE name = "choke" and spellid = 237;
UPDATE spell_list SET VE = 300 WHERE name = "rasp" and spellid = 238;
UPDATE spell_list SET VE = 300 WHERE name = "shock" and spellid = 239;
UPDATE spell_list SET VE = 300 WHERE name = "drown" and spellid = 240;
UPDATE spell_list SET VE = 480 WHERE name = "blaze_spikes" and spellid = 249;
UPDATE spell_list SET VE = 480 WHERE name = "ice_spikes" and spellid = 250;
UPDATE spell_list SET VE = 480 WHERE name = "shock_spikes" and spellid = 251;
UPDATE spell_list SET VE = 640 WHERE name = "absorb-str" and spellid = 266;
UPDATE spell_list SET VE = 640 WHERE name = "absorb-dex" and spellid = 267;
UPDATE spell_list SET VE = 640 WHERE name = "absorb-vit" and spellid = 268;
UPDATE spell_list SET VE = 640 WHERE name = "absorb-agi" and spellid = 269;
UPDATE spell_list SET VE = 640 WHERE name = "absorb-int" and spellid = 270;
UPDATE spell_list SET VE = 640 WHERE name = "absorb-mnd" and spellid = 271;
UPDATE spell_list SET VE = 640 WHERE name = "absorb-chr" and spellid = 272;
UPDATE spell_list SET CE = 320 WHERE name = "absorb-tp" and spellid = 275;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "fire_spirit" and spellid = 288;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "ice_spirit" and spellid = 289;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "air_spirit" and spellid = 290;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "earth_spirit" and spellid = 291;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "thunder_spirit" and spellid = 292;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "water_spirit" and spellid = 293;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "light_spirit" and spellid = 294;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "dark_spirit" and spellid = 295;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "carbuncle" and spellid = 296;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "fenrir" and spellid = 297;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "ifrit" and spellid = 298;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "titan" and spellid = 299;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "leviathan" and spellid = 300;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "garuda" and spellid = 301;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "shiva" and spellid = 302;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "ramuh" and spellid = 303;
UPDATE spell_list SET CE = 90, VE = 300 WHERE name = "diabolos" and spellid = 304;
UPDATE spell_list SET VE = 300 WHERE name = "utsusemi_ichi" and spellid = 338;
UPDATE spell_list SET VE = 300 WHERE name = "utsusemi_ni" and spellid = 339;
UPDATE spell_list SET CE = 80, VE = 240 WHERE name = "jubaku_ichi" and spellid = 341;
UPDATE spell_list SET CE = 80, VE = 240 WHERE name = "jubaku_ni" and spellid = 342;
UPDATE spell_list SET CE = 80, VE = 240 WHERE name = "hojo_ichi" and spellid = 344;
UPDATE spell_list SET CE = 80, VE = 240 WHERE name = "hojo_ni" and spellid = 345;
UPDATE spell_list SET CE = 80, VE = 240 WHERE name = "kurayami_ichi" and spellid = 347;
UPDATE spell_list SET CE = 80, VE = 240 WHERE name = "kurayami_ni" and spellid = 348;
UPDATE spell_list SET CE = 80, VE = 240 WHERE name = "dokumori_ichi" and spellid = 350;
UPDATE spell_list SET VE = 240 WHERE name = "tonko_ichi" and spellid = 353;
UPDATE spell_list SET VE = 240 WHERE name = "tonko_ni" and spellid = 354;
UPDATE spell_list SET CE = 0, VE = 0 WHERE name = "cure_v" and spellid = 5;

-- Carnage Blade additional effect
INSERT INTO `item_mods` VALUES (16827,431,1);

-- Toreador's Cape hidden effect
INSERT INTO `item_mods` VALUES (15465,166,50);

-- Fixing call beast mob animation
UPDATE mob_skills set mob_anim_id = 0 WHERE mob_skill_name = "call_beast" AND mob_skill_id = 1017;

-- Adding missing abilitites for Draugar
INSERT INTO `mob_skill_lists` VALUES('Draugar', 89, 478);
INSERT INTO `mob_skill_lists` VALUES('Draugar', 89, 479);
INSERT INTO `mob_skill_lists` VALUES('Draugar', 89, 484);
INSERT INTO `mob_skill_lists` VALUES('Draugar', 89, 1156);
INSERT INTO `mob_skill_lists` VALUES('Draugar', 89, 1795);

-- Enmity added to AOE abitilies
UPDATE abilities SET message1 = 116 WHERE abilityId = 32 AND name = "warcry";
UPDATE abilities SET message1 = 131 WHERE abilityId = 47 AND name = "holy_circle";
UPDATE abilities SET message1 = 134 WHERE abilityId = 50 AND name = "arcane_circle";
UPDATE abilities SET message1 = 148 WHERE abilityId = 64 AND name = "warding_circle";
UPDATE abilities SET message1 = 150 WHERE abilityId = 65 AND name = "ancient_circle";
UPDATE abilities SET message1 = 319 WHERE abilityId = 92 AND name = "rampart";
UPDATE abilities SET message1 = 441 WHERE abilityId = 162 AND name = "killer_instinct";

-- Dynamis mob_spawnpoints adjustments
UPDATE mob_spawn_points SET pos_x = 1.000, pos_y = 1.000, pos_z = 1.000 WHERE mobid = 17535124 AND mobname = "Vanguard_Grappler";
UPDATE mob_spawn_points SET mobname = "Warchief_Tombstone", polutils_name = "Warchief Tombstone", groupid = 53 WHERE mobid = 17535463 AND mobname = "Serjeant_Tombstone";
UPDATE mob_spawn_points SET mobname = "Warchief_Tombstone", polutils_name = "Warchief Tombstone", groupid = 53 WHERE mobid = 17535533 AND mobname = "Serjeant_Tombstone";
UPDATE mob_spawn_points SET mobname = "Warchief_Tombstone", polutils_name = "Warchief Tombstone", groupid = 53 WHERE mobid = 17535540 AND mobname = "Serjeant_Tombstone";
UPDATE mob_spawn_points SET mobname = "Warchief_Tombstone", polutils_name = "Warchief Tombstone", groupid = 53 WHERE mobid = 17535619 AND mobname = "Serjeant_Tombstone";
UPDATE mob_spawn_points SET mobname = "Warchief_Tombstone", polutils_name = "Warchief Tombstone", groupid = 53 WHERE mobid = 17535622 AND mobname = "Serjeant_Tombstone";
UPDATE mob_spawn_points SET mobname = "Warchief_Tombstone", polutils_name = "Warchief Tombstone", groupid = 53 WHERE mobid = 17535744 AND mobname = "Serjeant_Tombstone";

-- Dynamis mob_skill_lists adjustments
INSERT INTO `mob_skill_lists` VALUES('WarlordRojgnojOrcNM', 1166, 605);
INSERT INTO `mob_skill_lists` VALUES('WarlordRojgnojOrcNM', 1166, 606);
INSERT INTO `mob_skill_lists` VALUES('WarlordRojgnojOrcNM', 1166, 607);
INSERT INTO `mob_skill_lists` VALUES('WarlordRojgnojOrcNM', 1166, 608);
INSERT INTO `mob_skill_lists` VALUES('WarlordRojgnojOrcNM', 1166, 609);
INSERT INTO `mob_skill_lists` VALUES('WarlordRojgnojOrcNM', 1166, 1066);

-- Dynamis mob_pools adjustments
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Battlechoir_Gitchfotch";
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Reapertongue_Gadgquok";
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Soulsender_Fugbrag";
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Voidstreaker_Butchnotch";
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Wyrmgnasher_Bjakdek";
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Bladeburner_Rokgevok";
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Steelshank_Kratzvatz";
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Bloodfist_Voshgrosh";
UPDATE mob_pools SET skill_list_id = 1166 WHERE name = "Spellspear_Djokvukk";

UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Alchemist" and poolid = 4133;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Ambusher" and poolid = 4134;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Amputator" and poolid = 4135;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Armorer" and poolid = 4136;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Assassin" and poolid = 4137;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Backstabber" and poolid = 4138;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Beasttender" and poolid = 4139;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Bugler" and poolid = 4140;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Chanter" and poolid = 4141;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Constable" and poolid = 4142;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Defender" and poolid = 4143;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Dollmaster" and poolid = 4144;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Dragon" and poolid = 4145;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Drakekeeper" and poolid = 4146;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Enchanter" and poolid = 4147;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Exemplar" and poolid = 4148;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Eye" and poolid = 4149;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Footsoldier" and poolid = 4150;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Grappler" and poolid = 4151;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Gutslasher" and poolid = 4152;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Hatamoto" and poolid = 4153;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Hawker" and poolid = 4154;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Hitman" and poolid = 4155;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Impaler" and poolid = 4156;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Inciter" and poolid = 4157;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Kusa" and poolid = 4158;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Liberator" and poolid = 4159;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Maestro" and poolid = 4160;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Mason" and poolid = 4161;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Mesmerizer" and poolid = 4162;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Militant" and poolid = 4163;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Minstrel" and poolid = 4164;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Neckchopper" and poolid = 4165;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Necromancer" and poolid = 4166;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Ogresoother" and poolid = 4167;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Oracle" and poolid = 4168;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Partisan" and poolid = 4169;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Pathfinder" and poolid = 4170;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Persecutor" and poolid = 4171;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Pillager" and poolid = 4172;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Pitfighter" and poolid = 4173;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Predator" and poolid = 4174;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Prelate" and poolid = 4175;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Priest" and poolid = 4176;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Protector" and poolid = 4177;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Purloiner" and poolid = 4178;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Ronin" and poolid = 4179;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Salvager" and poolid = 4180;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Sentinel" and poolid = 4181;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Shaman" and poolid = 4182;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Skirmisher" and poolid = 4183;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Smithy" and poolid = 4184;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Thaumaturge" and poolid = 4191;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Tinkerer" and poolid = 4192;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Trooper" and poolid = 4193;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Undertaker" and poolid = 4194;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Vexer" and poolid = 4195;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Vigilante" and poolid = 4196;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Vindicator" and poolid = 4197;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Visionary" and poolid = 4198;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Welldigger" and poolid = 4199;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Dragontamer" and poolid = 4649;
UPDATE mob_pools SET mobType = 2 WHERE name = "Adamantking_Effigy" and poolid = 43;
UPDATE mob_pools SET mobType = 2 WHERE name = "Adamantking_Image" and poolid = 5111;
UPDATE mob_pools SET mobType = 2 WHERE name = "Arch_GuDha_Effigy" and poolid = 2284;
UPDATE mob_pools SET mobType = 2 WHERE name = "BuBho_Truesteel" and poolid = 6063;
UPDATE mob_pools SET mobType = 2 WHERE name = "GiPha_Manameister" and poolid = 1614;
UPDATE mob_pools SET mobType = 2 WHERE name = "GuDha_Effigy" and poolid = 1855;
UPDATE mob_pools SET mobType = 2 WHERE name = "GuNhi_Noondozer" and poolid = 1859;
UPDATE mob_pools SET mobType = 2 WHERE name = "KoDho_Cannonball" and poolid = 2285;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vazhe_Pummelsong" and poolid = 6062;
UPDATE mob_pools SET mobType = 2 WHERE name = "ZeVho_Fallsplitter" and poolid = 4499;
UPDATE mob_pools SET mobType = 2 WHERE name = "Goblin_Replica" and poolid = 1707;
UPDATE mob_pools SET mobType = 2 WHERE name = "Avatar_Icon" and poolid = 294;
UPDATE mob_pools SET mobType = 2 WHERE name = "Adamantking_Effigy" and poolid = 43;
UPDATE mob_pools SET mobType = 2 WHERE name = "Serjeant_Tombstone" and poolid = 3548;

-- Drop Rates
UPDATE mob_droplist SET itemRate = "1000" WHERE itemID = "16968" AND dropType != 2  AND dropType != 4  AND dropID  = "72"; -- Kamewari
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "902" AND dropType != 2  AND dropType != 4  AND dropID  = "1623"; -- *demon_horn* from *Marquis_Amon* was *110*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "902" AND dropType != 2  AND dropType != 4  AND dropID  = "1622"; -- *demon_horn* from *Marquis_Allocen* was *110*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "902" AND dropType != 2  AND dropType != 4  AND dropID  = "1213"; -- *demon_horn* from *Grand_Duke_Batym* was *110*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "902" AND dropType != 2  AND dropType != 4  AND dropID  = "716"; -- *demon_horn* from *Duke_Haborym* was *327*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "902" AND dropType != 2  AND dropType != 4  AND dropID  = "2582"; -- *demon_horn* from *Viscount_Morax* was *300*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "902" AND dropType != 2  AND dropType != 4  AND dropID  = "232"; -- *demon_horn* from *Baron_Vapula* was *110*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "902" AND dropType != 2  AND dropType != 4  AND dropID  = "229"; -- *demon_horn* from *Baronet_Romwe* was *110*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "4387" AND dropType != 2  AND dropType != 4  AND dropID  = "1173"; -- *wild_onion* from *Goblin_Thug* was *100*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "4387" AND dropType != 2  AND dropType != 4  AND dropID  = "1174"; -- *wild_onion* from *Goblin_Thug* was *210*


UPDATE mob_pools SET cmbDelay = 180, spellList = 0, mJob = 6, immunity = 29, entityFlags = 133 WHERE name = "King_Behemoth"; -- Faster attack speed, force usaage of meteor instead of spelllist, job thf/war
UPDATE mob_pools SET cmbDelay = 200, mJob = 6 WHERE name = "Behemoth"; -- Faster attack speed, force usaage of meteor instead of spelllist, job thf/war
UPDATE mob_groups SET HP = 65000 WHERE zoneid = 127 AND name = "Behemoth";
UPDATE mob_groups SET HP = 80000, MP = 20000 WHERE zoneid = 127 AND name = "King_Behemoth";
UPDATE mob_family_system SET speed = 60 WHERE family = "King_Behemoth"; -- Bigger model and faster speed

UPDATE mob_pools SET cmbDelay = 180, immunity = 32 WHERE name = "Voluptuous_Vivian"; -- Slight increase attack speed