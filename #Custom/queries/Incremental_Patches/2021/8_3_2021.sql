-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

UPDATE mob_skills SET mob_skill_aoe = 4 WHERE mob_skill_name = "jettatura" AND mob_skill_id = 577; -- Jettatura should be conal aoe


INSERT IGNORE INTO mob_skill_lists VALUES ('Qultada',145,39); -- Qultada WS Spirits Within 
INSERT IGNORE INTO mob_skill_lists VALUES ('Qultada',145,34); -- Qultada WS red Lotus Blade
INSERT IGNORE INTO mob_skill_lists VALUES ('Qultada',145,33); -- Qultada WS burning blade

UPDATE mob_groups SET HP = "3500" WHERE name = "Poisonhand_Gnadgad" and zoneid = "149"; -- Correct Poionshand from 1200hp to 3500hp


UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "16480" AND dropType != 2  AND dropType != 4  AND dropID  = "2298"; -- *thiefs_knife* from *Sozu_Rogberry* was *15*  ****** CHanged from 15 >> 10 to match new TH system.

UPDATE npc_list SET status = 0 WHERE npcid = "17776728" AND polutils_name = "Home Point #2"; -- Upper Jeuno
UPDATE npc_list SET status = 0 WHERE npcid = "17772800" AND polutils_name = "Home Point #2"; -- Ru'Lude Gardens
UPDATE npc_list SET status = 0 WHERE npcid = "17784890" AND polutils_name = "Home Point #2"; -- Port Jeuno
UPDATE npc_list SET status = 0 WHERE npcid = "17809460" AND polutils_name = "Home Point #2"; -- Norg
UPDATE npc_list SET status = 0 WHERE npcid = "17797161" AND polutils_name = "Home Point #1"; -- Mhaura
UPDATE npc_list SET status = 0 WHERE npcid = "17780874" AND polutils_name = "Home Point #2"; -- Lower Jeuno
UPDATE npc_list SET status = 0 WHERE npcid = "17801305" AND polutils_name = "Home Point #1"; -- Kazham

UPDATE item_basic SET basesell = 1 WHERE itemid = 4510; -- Acorn Cookies, was 6 gil. Nerfed to avoid gil exploit through crafting.