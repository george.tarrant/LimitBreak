-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- -----------
-- GENERAL  --
-- -----------

-- Level Restriction fix
UPDATE status_effects SET flags = "276824064", remove_id = "269" WHERE name = "level_restriction";

-- --------------------------------
-- Dynamis-Bastok SQL Drop Tables --
-- --------------------------------

-- Dynamis Bastok

-- Normal Beastmen Mobs - Vanguards (2558)
DELETE FROM mob_droplist WHERE dropid = "2558" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15133, 77); -- Adds MNK Feet
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15120, 77); -- Adds BLM Legs
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15106, 77); -- Adds RDM Hands
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15092, 77); -- Adds THF Body
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15078, 77); -- Adds PLD Head
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15139, 77); -- Adds DRK Feet
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15095, 77); -- Adds BST Body
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15111, 77); -- Adds BRD Hands
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15113, 77); -- Adds SAM Hands
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15130, 77); -- Adds DRG Legs
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15116, 77); -- Adds SMN Hands
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 16346, 77); -- Adds BLU Legs
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 11385, 76); -- Adds COR Feet
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1469, 10); -- Adds chunk_of_wootz_ore
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1521, 10); -- Adds vial_of_slime_juice
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1455, 100); -- Adds 1 bynes 
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1455, 100); -- Adds 2 bynes 
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1455, 100); -- Adds 3 bynes
INSERT INTO mob_droplist VALUES(2558, 1, 2, 50, 18314, 250); -- Adds Ito
INSERT INTO mob_droplist VALUES(2558, 1, 2, 50, 18302, 250); -- Adds relic scythe
INSERT INTO mob_droplist VALUES(2558, 1, 2, 50, 18284, 250); -- Adds relic axe
INSERT INTO mob_droplist VALUES(2558, 1, 2, 50, 18278, 250); -- Adds relic blade

-- NM Beastmen Mobs - 2907
DELETE FROM mob_droplist WHERE dropid = "2907" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15133, 77); -- Adds MNK Feet
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15120, 77); -- Adds BLM Legs
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15106, 77); -- Adds RDM Hands
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15092, 77); -- Adds THF Body
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15078, 77); -- Adds PLD Head
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15139, 77); -- Adds DRK Feet
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15095, 77); -- Adds BST Body
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15111, 77); -- Adds BRD Hands
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15113, 77); -- Adds SAM Hands
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15130, 77); -- Adds DRG Legs
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15116, 77); -- Adds SMN Hands
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 16346, 77); -- Adds BLU Legs
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 11385, 76); -- Adds COR Feet
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1469, 10); -- Adds chunk_of_wootz_ore
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1521, 10); -- Adds vial_of_slime_juice
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1455, 100); -- Adds 1 bynes 
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1455, 100); -- Adds 2 bynes 
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1455, 100); -- Adds 3 bynes
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1456, 50); -- Adds 1 Hbyne

-- Effigy - 20
DELETE FROM mob_droplist WHERE dropid = "20"; -- Delete
INSERT INTO mob_droplist VALUES(20, 0, 0, 1000, 749, 50); -- Add myth coin
INSERT INTO mob_droplist VALUES(20, 0, 0, 1000, 748, 50); -- Add gold coin
INSERT INTO mob_droplist VALUES(20, 0, 0, 1000, 1474, 100); -- Add infinity core
INSERT INTO mob_droplist VALUES(20, 0, 0, 1000, 1456, 50); -- Adds 1 Hbyne

-- Megaboss - 2906
DELETE FROM mob_droplist WHERE dropid = "2906"; -- Delete
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 749, 100); -- Add myth coin
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 748, 100); -- Add gold coin
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 1474, 150); -- Add infinity core
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 1455, 150); -- Adds 1 bynes 
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 1455, 150); -- Adds 2 bynes 
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 1456, 150); -- Adds 1 Hbyne

-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "20" WHERE zoneid = "186" AND name = "Adamantking_Effigy";
UPDATE mob_groups SET dropid = "20" WHERE zoneid = "186" AND name = "Adamantking_Image";
UPDATE mob_groups SET dropid = "2906" WHERE zoneid = "186" AND name = "Arch_GuDha_Effigy";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "BuBho_Truesteel";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "GiPha_Manameister";
UPDATE mob_groups SET dropid = "2906" WHERE zoneid = "186" AND name = "GuDha_Effigy";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "GuNhi_Noondozer";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "KoDho_Cannonball";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "186" AND name = "RaGhos_Avatar";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "RaGho_Darkfount";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "186" AND name = "Vanguards_Avatar";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "186" AND name = "Vanguards_Scorpion";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "186" AND name = "Vanguards_Wyvern";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Beasttender";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Constable";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Defender";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Drakekeeper";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Hatamoto";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Kusa";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Mason";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Militant";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Minstrel";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Protector";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Purloiner";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Thaumaturge";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Undertaker";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Vigilante";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Vindicator";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "Vazhe_Pummelsong";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "ZeVho_Fallsplitter";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "ZoPha_Forgesoul";

-- --------------------------------
-- Dynamis-Jeuno SQL Drop Tables --
-- --------------------------------

-- Dynamis Jueno

-- Normal Beastmen Mobs - Vanguards (2543)
DELETE FROM mob_droplist WHERE dropid = "2543" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15102, 72); -- Adds WAR Hands
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15103, 72); -- Adds MNK Hands
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15119, 72); -- Adds WHM Legs
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15135, 72); -- Adds BLM Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15121, 72); -- Adds RDM Legs
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15137, 72); -- Adds THF Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15124, 71); -- Adds DRK Legs
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15141, 71); -- Adds BRD Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15082, 71); -- Adds RNG Head
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15143, 71); -- Adds SAM Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15144, 71); -- Adds NIN Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15115, 71); -- Adds DRG Hands
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15028, 71); -- Adds COR Hands
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 16352, 71); -- Adds PUP Legs
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1520, 10); -- Adds goblin grease
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1455, 100); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1449, 100); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1452, 100); -- Adds 1 coin
INSERT INTO mob_droplist VALUES(2543, 1, 2, 50, 18344, 250); -- Adds relic bow
INSERT INTO mob_droplist VALUES(2543, 1, 2, 50, 18338, 250); -- Adds relic horn 
INSERT INTO mob_droplist VALUES(2543, 1, 2, 50, 18326, 250); -- Adds relic staff
INSERT INTO mob_droplist VALUES(2543, 1, 2, 50, 15066, 250); -- Adds relic shield

-- NM Beastmen Mobs
DELETE FROM mob_droplist WHERE dropid = "143" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15102, 72); -- Adds WAR Hands
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15103, 72); -- Adds MNK Hands
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15119, 72); -- Adds WHM Legs
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15135, 72); -- Adds BLM Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15121, 72); -- Adds RDM Legs
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15137, 72); -- Adds THF Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15124, 71); -- Adds DRK Legs
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15141, 71); -- Adds BRD Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15082, 71); -- Adds RNG Head
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15143, 71); -- Adds SAM Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15144, 71); -- Adds NIN Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15115, 71); -- Adds DRG Hands
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15028, 71); -- Adds COR Hands
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 16352, 71); -- Adds PUP Legs
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1520, 10); -- Adds goblin grease
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1455, 100); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1449, 100); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1452, 100); -- Adds 1 coin 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1455, 50); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1449, 50); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1452, 50); -- Adds 1 coin 
INSERT INTO mob_droplist VALUES(143, 1, 2, 50, 1456, 333); -- Adds 1 H byne 
INSERT INTO mob_droplist VALUES(143, 1, 2, 50, 1450, 333); -- Adds 1 Lungo 
INSERT INTO mob_droplist VALUES(143, 1, 2, 50, 1453, 334); -- Adds 1 Mont

-- Replicas
DELETE FROM mob_droplist WHERE dropid = "1144"; -- Delete
INSERT INTO mob_droplist VALUES(1144, 0, 0, 1000, 749, 50); -- Add myth coin
INSERT INTO mob_droplist VALUES(1144, 0, 0, 1000, 748, 50); -- Add gold coin
INSERT INTO mob_droplist VALUES(1144, 0, 0, 1000, 1474, 100); -- Add infinity core
INSERT INTO mob_droplist VALUES(1144, 0, 0, 1000, 1470, 50); -- Add sparkling stone
INSERT INTO mob_droplist VALUES(1144, 1, 1, 50, 1456, 333); -- Adds 1 Hbill 
INSERT INTO mob_droplist VALUES(1144, 1, 1, 50, 1450, 333); -- Adds 1 Lungo 
INSERT INTO mob_droplist VALUES(1144, 1, 1, 50, 1453, 334); -- Adds 1 Mont

-- Megaboss
DELETE FROM mob_droplist WHERE dropid = "1085"; -- Delete
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 749, 100); -- Add myth coin
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 748, 100); -- Add gold coin
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1474, 150); -- Add infinity core
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1470, 50); -- Add sparkling stone
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1455, 150); -- add single bill 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1449, 150); -- add single shell 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1455, 150); -- add single coin 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1456, 100); -- add Hbill 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1450, 100); -- add Lungo
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1453, 100); -- add Mont 

-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Smithy";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Alchemist";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Ambusher";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Armorer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Dragontamer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Enchanter";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Hitman";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Maestro";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Necromancer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Pathfinder";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Pitfighter";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Ronin";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Shaman";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Tinkerer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Welldigger";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Anvilix_Sootwrists";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Bandrix_Rockjaw";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Blazox_Boneybod";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Bootrix_Jaggedelbow";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Buffrix_Eargone";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Cloktix_Longnail";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Distilix_Stickytoes";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Elixmix_Hooknose";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Eremix_Snottynostril";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Feralox_Honeylips";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "188" AND name ="Feraloxs_Slime";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Gabblox_Magpietongue";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Hermitrix_Toothrot";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Humnox_Drumbelly";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Jabbrox_Grannyguise";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Jabkix_Pigeonpecs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Karashix_Swollenskull";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Kikklix_Longlegs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Lurklox_Dhalmelneck";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Mobpix_Mucousmouth";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Morgmox_Moldnoggin";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Mortilox_Wartpaws";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Prowlox_Barrelbelly";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Quicktrix_Hexhands";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Rutrix_Hamgams";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Scourquix_Scaleskin";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "188" AND name ="Scourquixs_Wyvern";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Scruffix_Shaggychest";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Slystix_Megapeepers";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Smeltix_Thickhide";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Snypestix_Eaglebeak";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Sparkspox_Sweatbrow";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Ticktox_Beadyeyes";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Trailblix_Goatmug";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Tufflix_Loglimbs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Tymexox_Ninefingers";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wasabix_Callusdigit";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wilywox_Tenderpalm";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wyrmwix_Snakespecs";
UPDATE mob_groups SET dropid = "1085" WHERE zoneid = "188" AND name ="Goblin_Golem";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Replica";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Statue";

-- --------------------------------
-- Dynamis-Sandoria SQL Drop Tables --
-- --------------------------------

-- Dynamis Sandoria

-- Normal Beastmen Mobs - Vanguards (2548)
DELETE FROM mob_droplist WHERE dropid = "2548" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15132, 72); -- Adds WAR Feet
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15118, 72); -- Adds MNK Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15074, 72); -- Adds WHM Head
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15136, 72); -- Adds RDM Feet
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15108, 72); -- Adds PLD Hands
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15125, 72); -- Adds BST Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15081, 71); -- Adds BRD Head
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15127, 71); -- Adds RNG Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15129, 71); -- Adds Nin Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15145, 71); -- Adds DRG Feet
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15146, 71); -- Adds SMN Feet
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15025, 71); -- Adds BLU Hands
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 16349, 71); -- Adds COR Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 11388, 71); -- Adds PUP Feet
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1519, 10); -- Adds fresh_orc_liver
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1517, 10); -- Adds giant_frozen_head
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1516, 10); -- Adds griffon_hide
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1452, 100); -- Adds 1 bronzepiece 
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1452, 100); -- Adds 2 bronzepiece 
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1452, 100); -- Adds 3 bronzepiece
INSERT INTO mob_droplist VALUES(2548, 1, 2, 50, 18308, 250); -- Adds ihintanto
INSERT INTO mob_droplist VALUES(2548, 1, 2, 50, 18332, 250); -- Adds relic gun	
INSERT INTO mob_droplist VALUES(2548, 1, 2, 50, 18290, 250); -- Adds relic bhuj
INSERT INTO mob_droplist VALUES(2548, 1, 2, 50, 18296, 250); -- Adds relic lance

-- NM Beastmen Mobs - 237
DELETE FROM mob_droplist WHERE dropid = "237" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15132, 72); -- Adds WAR Feet
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15118, 72); -- Adds MNK Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15074, 72); -- Adds WHM Head
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15136, 72); -- Adds RDM Feet
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15108, 72); -- Adds PLD Hands
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15125, 72); -- Adds BST Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15081, 71); -- Adds BRD Head
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15127, 71); -- Adds RNG Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15129, 71); -- Adds Nin Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15145, 71); -- Adds DRG Feet
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15146, 71); -- Adds SMN Feet
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15025, 71); -- Adds BLU Hands
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 16349, 71); -- Adds COR Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 11388, 71); -- Adds PUP Feet
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1519, 10); -- Adds fresh_orc_liver
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1517, 10); -- Adds giant_frozen_head
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1516, 10); -- Adds griffon_hide
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1452, 100); -- Adds 1 bronzepiece 
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1452, 100); -- Adds 2 bronzepiece
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1452, 100); -- Adds 3 bronzepiece
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1453, 50); -- Adds 1 Mont

-- Tombstones - 2201
DELETE FROM mob_droplist WHERE dropid = "2201"; -- Delete
INSERT INTO mob_droplist VALUES(2201, 0, 0, 1000, 749, 50); -- Add myth coin
INSERT INTO mob_droplist VALUES(2201, 0, 0, 1000, 748, 50); -- Add gold coin
INSERT INTO mob_droplist VALUES(2201, 0, 0, 1000, 1474, 100); -- Add infinity core
INSERT INTO mob_droplist VALUES(2201, 0, 0, 1000, 1453, 50); -- Adds 1 Mont

-- Megaboss - 1967
DELETE FROM mob_droplist WHERE dropid = "1967"; -- Delete
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 749, 100); -- Add myth coin
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 748, 100); -- Add gold coin
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 1474, 150); -- Add infinity core
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 1452, 150); -- Adds 1 bronzepiece 
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 1452, 150); -- Adds 2 bronzepiece 
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 1453, 150); -- Adds 1 Mont

-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "1967" WHERE zoneid = "185" AND name = "Arch_Overlord_Tombstone";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Battlechoir_Gitchfotch";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Bladeburner_Rokgevok";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Bloodfist_Voshgrosh";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "185" AND name = "Djokvukks_Wyvern";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Kratzvatzs_Hecteyes";
UPDATE mob_groups SET dropid = "1967" WHERE zoneid = "185" AND name = "Overlords_Tombstone";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Reapertongue_Gadgquok";
UPDATE mob_groups SET dropid = "2201" WHERE zoneid = "185" AND name = "Serjeant_Tombstone";
UPDATE mob_groups SET dropid = "2201" WHERE zoneid = "185" AND name = "Serjeant_Tombstone";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Soulsender_Fugbrag";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Spellspear_Djokvukk";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Steelshank_Kratzvatz";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "185" AND name = "Vanguards_Avatar";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "185" AND name = "Vanguards_Hecteyes";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "185" AND name = "Vanguards_Wyvern";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Amputator";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Backstabber";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Bugler";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Dollmaster";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Footsoldier";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Grappler";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Gutslasher";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Hawker";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Impaler";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Mesmerizer";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Neckchopper";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Pillager";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Predator";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Trooper";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Vexer";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Voidstreaker_Butchnotch";
UPDATE mob_groups SET dropid = "2201" WHERE zoneid = "185" AND name = "Warchief_Tombstone";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Wyrmgnasher_Bjakdek";

-- --------------------------------
-- Dynamis-Windurst SQL Drop Tables --
-- --------------------------------

-- Dynamis Windurst

-- Normal Beastmen Mobs - Vanguards (2553)
DELETE FROM mob_droplist WHERE dropid = "2553" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15072, 77); -- Adds WAR Head
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15134, 77); -- Adds WHM Feet
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15105, 77); -- Adds BLM Hands
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15077, 77); -- Adds THF Head
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15138, 77); -- Adds PLD Feet
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15109, 77); -- Adds DRK Hands
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15080, 77); -- Adds BST Head
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15112, 77); -- Adds RNG Hands
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15128, 77); -- Adds Sam Legs
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15084, 77); -- Adds Nin Head
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15131, 77); -- Adds SMN Legs
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 11382, 77); -- Adds BLU Feet
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15031, 76); -- Adds PUP Hands
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1518, 10); -- Adds colossal skull
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1466, 10); -- Adds relic iron
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1464, 10); -- Adds lancewood log
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1449, 100); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1449, 100); -- Adds 2 shell 
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1449, 100); -- Adds 3 shell
INSERT INTO mob_droplist VALUES(2553, 1, 2, 50, 18266, 250); -- Adds relic dagger
INSERT INTO mob_droplist VALUES(2553, 1, 2, 50, 18260, 250); -- Adds relic knuckles
INSERT INTO mob_droplist VALUES(2553, 1, 2, 50, 18320, 250); -- Adds relic maul
INSERT INTO mob_droplist VALUES(2553, 1, 2, 50, 18272, 250); -- Adds relic sword

-- NM Beastmen Mobs - 1560
DELETE FROM mob_droplist WHERE dropid = "1560" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15072, 77); -- Adds WAR Head
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15134, 77); -- Adds WHM Feet
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15105, 77); -- Adds BLM Hands
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15077, 77); -- Adds THF Head
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15138, 77); -- Adds PLD Feet
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15109, 77); -- Adds DRK Hands
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15080, 77); -- Adds BST Head
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15112, 77); -- Adds RNG Hands
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15128, 77); -- Adds Sam Legs
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15084, 77); -- Adds Nin Head
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15131, 77); -- Adds SMN Legs
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 11382, 77); -- Adds BLU Feet
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15031, 76); -- Adds PUP Hands
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1518, 10); -- Adds colossal skull
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1466, 10); -- Adds relic iron
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1464, 10); -- Adds lancewood log
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1449, 100); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1449, 100); -- Adds 2 shell 
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1449, 100); -- Adds 3 shell
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1450, 50); -- Adds 1 Lungo 

-- Icons - 195
DELETE FROM mob_droplist WHERE dropid = "195"; -- Delete
INSERT INTO mob_droplist VALUES(195, 0, 0, 1000, 749, 50); -- Add myth coin
INSERT INTO mob_droplist VALUES(195, 0, 0, 1000, 748, 50); -- Add gold coin
INSERT INTO mob_droplist VALUES(195, 0, 0, 1000, 1474, 100); -- Add infinity core
INSERT INTO mob_droplist VALUES(195, 0, 0, 1000, 1450, 50); -- Adds 1 Lungo 

-- Megaboss - 2510
DELETE FROM mob_droplist WHERE dropid = "2510"; -- Delete
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 749, 100); -- Add myth coin
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 748, 100); -- Add gold coin
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 1474, 150); -- Add infinity core
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 1449, 150); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 1449, 150); -- Adds 2 shell 
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 1450, 150); -- Adds 1 Lungo 

-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Arch_Tzee_Xicu_Idol";
UPDATE mob_groups SET dropid = "195" WHERE zoneid = "187" AND name = "Avatar_Icon";
UPDATE mob_groups SET dropid = "195" WHERE zoneid = "187" AND name = "Avatar_Idol";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Fuu_Tzapo_the_Blessed";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Haa_Pevi_the_Stentorian";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Loo_Hepe_the_Eyepiercer";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Maa_Febi_the_Steadfast";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Muu_Febi_the_Steadfast";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Naa_Yixo_the_Stillrage";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Tee_Zaksa_the_Ceaseless";
UPDATE mob_groups SET dropid = "2510" WHERE zoneid = "187" AND name = "Tzee_Xicu_Idol";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "187" AND name = "Vanguards_Avatar";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "187" AND name = "Vanguards_Crow";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "187" AND name = "Vanguards_Wyvern";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Assassin";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Chanter";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Exemplar";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Inciter";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Liberator";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Ogresoother";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Oracle";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Partisan";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Persecutor";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Prelate";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Priest";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Salvager";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Sentinel";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Skirmisher";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Visionary";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Wuu_Qoho_the_Razorclaw";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Xoo_Kaza_the_Solemn";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "187" AND name = "Xuu_Bhoqas_Avatar";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Xuu_Bhoqa_the_Enigma";

