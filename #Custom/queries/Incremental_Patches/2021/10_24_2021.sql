-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

-- Heims Earring drop update
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "14805" AND dropType != 2  AND dropType != 4  AND dropID  = "596"; -- *heims_earring* from *Defoliate Leshy * was *100*

-- Black Puddings missing loot
UPDATE mob_groups SET mob_groups.dropid = 740 WHERE name = "Black_Pudding";