-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

UPDATE mob_groups SET HP = 6500 WHERE name = "Edacious_Opo-opo" and zoneid = 124; -- Edacious Opo-opo HP adjustment

UPDATE mob_groups SET spawntype = 128 WHERE name = "Scimitar_Scorpion" and zoneid = 143; -- Mine Scorpion is now a PH for Scimitar Scorpion (Palborough Mines)
INSERT IGNORE INTO nm_spawn_points VALUES (17363241,0,257.723,-31.665,182.547);
INSERT IGNORE INTO nm_spawn_points VALUES (17363284,0,19.372,-32.156,80.410);

UPDATE mob_pools SET immunity = "17", spellList = 478, behavior = 0 WHERE name = "Old_Professor_Mariselle" AND poolid = 2964; -- Immune to sleep and silence & custom spell list. Fixes no_standack issue.

UPDATE guild_shops SET min_price = 280, max_price = 624, max_quantity = 32, daily_increase = 12, initial_quantity = 12 WHERE guildid = 60430 and itemid = 839; -- Guild was re-stocking too fast
UPDATE guild_shops SET min_price = 280, max_price = 624, max_quantity = 32, daily_increase = 12, initial_quantity = 12 WHERE guildid = 5152 and itemid = 839; -- Guild was re-stocking too fast
UPDATE guild_shops SET min_price = 280, max_price = 624, max_quantity = 32, daily_increase = 12, initial_quantity = 12 WHERE guildid = 516 and itemid = 839; -- Guild was re-stocking too fast

UPDATE mob_pools SET links = "0" WHERE name = "Azren_Kuba"; -- Should not link with each other
UPDATE mob_pools SET links = "0" WHERE name = "Azren_Kuguza"; -- Should not link with each other

UPDATE guild_shops SET max_quantity = 64, daily_increase = 24, initial_quantity = 24 WHERE guildid = 516 and itemid = 834; -- Guild was re-stocking too fast

UPDATE guild_shops SET max_quantity = 48, daily_increase = 24, initial_quantity = 24 WHERE guildid = 60430 and itemid = 816; -- Silk Thread restock too high
UPDATE guild_shops SET max_quantity = 48, daily_increase = 24, initial_quantity = 24 WHERE guildid = 516 and itemid = 816; -- Silk Thread restock too high


UPDATE npc_list SET pos_rot = 0, pos_x = 0, pos_y = 0, pos_z = 0 WHERE npcid = "17359089" AND polutils_name = "Scrape Mark"; -- OOE

DELETE FROM mob_spawn_mods WHERE mobid = "17428751"; -- Remove Sozu Rogberry 5minute idle despawn from old force pop system.
UPDATE mob_groups SET hp = "7000" WHERE name ="Sozu_Rogberry"; -- Increase Sozu Rogberry HP from 3k >> 7k

DELETE FROM mob_spawn_mods WHERE mobid = "17424444"; -- Remove Pallas 5minute idle despawn from old force pop system.
DELETE FROM mob_spawn_mods WHERE mobid = "17424480"; -- Remove Alkyoneus 5minute idle despawn from old force pop system.

UPDATE mob_groups SET HP = 5000 WHERE name = "Old_Professor_Mariselle"; -- Reset HP values for teleport working
UPDATE mob_groups SET HP = 1000 WHERE name = "Mariselles_Pupil"; -- Reset HP values for teleport working

DELETE FROM gardening_results WHERE resultid = 716; -- Remove OOE Winterflower from Gardening Results
