-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- -----------
-- GENERAL  --
-- -----------

-- CoP 6-2
UPDATE mob_pools SET immunity = 241 WHERE name = "Disaster_Idol" and poolid = 1049;
UPDATE mob_pools SET skill_list_id = 5003 WHERE name = "Gargoyle" and poolid = 1463;
UPDATE mob_pools SET skill_list_id = 5003 WHERE name = "Gargoyle" and poolid = 5158;
UPDATE mob_pools SET skill_list_id = 706 WHERE name = "Ponderer" and poolid = 3172;
UPDATE mob_pools SET skill_list_id = 5002 WHERE name = "Shikaree_Zs_Wyvern" and poolid = 3602;
UPDATE mob_pools SET skill_list_id = 1158 WHERE name = "Snoll_Tzar" and poolid = 3684;

INSERT IGNORE INTO `mob_skill_lists` VALUES('ShikareeWyv', 5002, 900);
INSERT IGNORE INTO `mob_skill_lists` VALUES('ShikareeWyv', 5002, 901);
INSERT IGNORE INTO `mob_skill_lists` VALUES('ShikareeWyv', 5002, 902);
INSERT IGNORE INTO `mob_skill_lists` VALUES('ShikareeWyv', 5002, 903);
INSERT IGNORE INTO `mob_skill_lists` VALUES('ShikareeWyv', 5002, 904);
INSERT IGNORE INTO `mob_skill_lists` VALUES('ShikareeWyv', 5002, 905);
INSERT IGNORE INTO `mob_skill_lists` VALUES('ShikareeWyv', 5002, 896);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Psoxja_Gargoyle', 5003, 538);

UPDATE mob_pools SET skill_list_id = 1165 WHERE name = "Warder_Aglaia" and poolid = 4291;
UPDATE mob_pools SET skill_list_id = 1165 WHERE name = "Warder_Euphrosyne" and poolid = 4292;
UPDATE mob_pools SET skill_list_id = 1165 WHERE name = "Warder_Thalia" and poolid = 4293;
INSERT IGNORE INTO `mob_skill_lists` VALUES('Cop_Warders', 1165, 561);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Cop_Warders', 1165, 562);
INSERT IGNORE INTO `mob_skills` VALUES(562, 972, 'reactive_armor', 0, 7.0, 2000, 1500, 1, 0, 0, 0, 0, 0, 0);

-- CoP Adjustments and Promy Vahzl
UPDATE mob_groups SET minLevel = 49, maxLevel = 51 WHERE name = "Wanderer" and groupid = 5 and zoneid = 22;
UPDATE mob_groups SET minLevel = 50, maxLevel = 52 WHERE name = "Weeper" and groupid = 6 and zoneid = 22;
UPDATE mob_groups SET minLevel = 54, maxLevel = 56 WHERE name = "Thinker" and groupid = 8 and zoneid = 22;
UPDATE mob_groups SET minLevel = 54, maxLevel = 57 WHERE name = "Gorger" and groupid = 9 and zoneid = 22;
UPDATE mob_groups SET minLevel = 54, maxLevel = 58 WHERE name = "Craver" and groupid = 10 and zoneid = 22;
UPDATE mob_groups SET minLevel = 50, maxLevel = 52 WHERE name = "Weeper" and groupid = 13 and zoneid = 22;
UPDATE mob_groups SET minLevel = 49, maxLevel = 51 WHERE name = "Wanderer" and groupid = 14 and zoneid = 22;
UPDATE mob_groups SET minLevel = 51, maxLevel = 53 WHERE name = "Seether" and groupid = 15 and zoneid = 22;
UPDATE mob_groups SET minLevel = 56, maxLevel = 58 WHERE name = "Thinker" and groupid = 16 and zoneid = 22;
UPDATE mob_groups SET minLevel = 56, maxLevel = 58 WHERE name = "Gorger" and groupid = 17 and zoneid = 22;
UPDATE mob_groups SET minLevel = 54, maxLevel = 58 WHERE name = "Craver" and groupid = 18 and zoneid = 22;
UPDATE mob_groups SET minLevel = 54, maxLevel = 56 WHERE name = "Weeper" and groupid = 21 and zoneid = 22;
UPDATE mob_groups SET minLevel = 58, maxLevel = 60 WHERE name = "Thinker" and groupid = 24 and zoneid = 22;
UPDATE mob_groups SET minLevel = 58, maxLevel = 60 WHERE name = "Gorger" and groupid = 25 and zoneid = 22;
UPDATE mob_groups SET minLevel = 58, maxLevel = 60 WHERE name = "Craver" and groupid = 26 and zoneid = 22;
UPDATE mob_groups SET minLevel = 52, maxLevel = 56 WHERE name = "Wanderer" and groupid = 28 and zoneid = 22;
UPDATE mob_groups SET minLevel = 54, maxLevel = 57 WHERE name = "Weeper" and groupid = 29 and zoneid = 22;
UPDATE mob_groups SET minLevel = 56, maxLevel = 58 WHERE name = "Seether" and groupid = 30 and zoneid = 22;
UPDATE mob_groups SET minLevel = 58, maxLevel = 60 WHERE name = "Thinker" and groupid = 32 and zoneid = 22;
UPDATE mob_groups SET minLevel = 58, maxLevel = 60 WHERE name = "Gorger" and groupid = 33 and zoneid = 22;
UPDATE mob_groups SET minLevel = 58, maxLevel = 60 WHERE name = "Craver" and groupid = 34 and zoneid = 22;

UPDATE mob_pools SET immunity = 16 WHERE name = "Caithleann";
UPDATE mob_spawn_points SET pos_rot = 62 WHERE mobname = "Snoll_Tzar";

-- Cop 7-4
UPDATE mob_groups SET HP = 14000 WHERE name = "Dalham";

UPDATE mob_pools SET spellList = 0, skill_list_id = 1167 WHERE name = "Boggelmann";
UPDATE mob_pools SET immunity = 17 WHERE name = "Cryptonberry_Assassin" and poolid = 847;
UPDATE mob_pools SET immunity = 17 WHERE name = "Cryptonberry_Executor" and poolid = 851;
UPDATE mob_pools SET immunity = 17, skill_list_id = 1168 WHERE name = "Dalham" and poolid = 895;
UPDATE mob_pools SET immunity = 17 WHERE name = "Cryptonberry_Assassin" and poolid = 4833;
UPDATE mob_pools SET immunity = 17 WHERE name = "Cryptonberry_Assassin" and poolid = 4834;

INSERT IGNORE INTO `mob_skill_lists` VALUES('Boggelmann', 1167, 382);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Boggelmann', 1167, 383);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Boggelmann', 1167, 384);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Boggelmann', 1167, 385);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Boggelmann', 1167, 386);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Boggelmann', 1167, 387);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Boggelmann', 1167, 1363);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Dalham', 1168, 456);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Dalham', 1168, 458);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Dalham', 1168, 459);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Dalham', 1168, 460);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Dalham', 1168, 461);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Dalham', 1168, 462);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Dalham', 1168, 463);
INSERT IGNORE INTO `mob_skill_lists` VALUES('Dalham', 1168, 626);

INSERT IGNORE INTO `mob_skills` VALUES(626, 437, 'vulture_3', 0, 7.0, 2000, 0, 1, 0, 0, 0, 0, 0, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1363, 822, 'hungry_crunch', 0, 7.0, 2000, 1500, 4, 0, 0, 0, 0, 0, 0);

-- CoP Prog until 7-5
UPDATE mob_pools SET immunity = 16386 WHERE name = "Mammet-22_Zeta";
UPDATE mob_pools SET immunity = 16385 WHERE name = "Omega" AND poolid = 2973;
UPDATE mob_pools SET immunity = 16385 WHERE name = "Ultima" AND poolid = 4083;

INSERT IGNORE INTO `mob_skills` VALUES(1238, 928, 'target_analysis', 0, 7.0, 2000, 1500, 4, 0, 0, 0, 0, 0, 0);

INSERT IGNORE INTO `mob_skill_lists` VALUES('Omega', 54, 1238);

-- CoP 7-5
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908311;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908312;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908313;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908315;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908316;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908317;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908319;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908320;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908321;

UPDATE mob_groups SET minLevel = 75, maxLevel = 75 WHERE name = "Tenzen" AND groupid = 4;
UPDATE mob_groups SET minLevel = 67, maxLevel = 67 WHERE name = "Makki-Chebukki" AND groupid = 5;
UPDATE mob_groups SET minLevel = 67, maxLevel = 67 WHERE name = "Kukki-Chebukki" AND groupid = 6;
UPDATE mob_groups SET minLevel = 67, maxLevel = 67 WHERE name = "Cherukiki" AND groupid = 7;

UPDATE mob_pools SET immunity = 25, spellList = 458, skill_list_id = 0 WHERE name = "Cherukiki" AND poolid = 710;
UPDATE mob_pools SET immunity = 25, spellList = 459, skill_list_id = 0 WHERE name = "Kukki-Chebukki" AND poolid = 2293;
UPDATE mob_pools SET immunity = 25, animationsub = 6, skill_list_id = 0 WHERE name = "Makki-Chebukki" AND poolid = 2492;
UPDATE mob_pools SET immunity = 16417, animationsub = 7 WHERE name = "Tenzen" AND poolid = 3875;

INSERT IGNORE INTO `mob_skill_lists` VALUES('Tenzen_Ranged', 1169, 1396);

DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_torimai";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_kazakiri";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_yukiarashi";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_tsukioboro";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_hanaikusa";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_tsukikage";
DELETE FROM mob_skills WHERE mob_skill_name = "tenzen_ranged_alt";
DELETE FROM mob_skills WHERE mob_skill_name = "oisoya";
DELETE FROM mob_skills WHERE mob_skill_name = "riceball";
DELETE FROM mob_skills WHERE mob_skill_name = "cosmic_elucidation";
INSERT IGNORE INTO `mob_skills` VALUES(1390, 1037, 'amatsu_torimai', 0, 15.0, 2000, 0, 4, 0, 0, 0, 1, 4, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1391, 1038, 'amatsu_kazakiri', 0, 15.0, 2000, 0, 4, 0, 0, 0, 4, 6, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1392, 1039, 'amatsu_yukiarashi', 0, 15.0, 2000, 0, 4, 0, 0, 0, 7, 6, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1393, 1040, 'amatsu_tsukioboro', 0, 15.0, 2000, 0, 4, 0, 0, 0, 10, 5, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1394, 1041, 'amatsu_hanaikusa', 0, 15.0, 2000, 0, 4, 0, 0, 0, 11, 2, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1395, 1036, 'amatsu_tsukikage', 0, 15.0, 2000, 0, 4, 0, 0, 0, 12, 6, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1396, 1034, 'tenzen_ranged_alt', 0, 25, 2000, 0, 4, 4, 0, 0, 0, 0, 0); -- tenzen ranged attack
INSERT IGNORE INTO `mob_skills` VALUES(1397, 1042, 'oisoya', 0, 15.0, 2000, 0, 4, 0, 0, 0, 0, 0, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1398, 1032, 'riceball', 0, 7.0, 2000, 0, 1, 0, 0, 0, 0, 0, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1399, 1035, 'cosmic_elucidation', 1, 15.0, 2000, 0, 4, 0, 0, 0, 0, 0, 0);

UPDATE mob_spawn_points SET pos_x = -639.985, pos_y = -231.347, pos_z = 525.910, pos_rot = 65	WHERE mobname = "Tenzen" AND mobid = 16908310;
UPDATE mob_spawn_points SET pos_x = 0.0000, pos_y = -151.347, pos_z = 166.000, pos_rot = 65	WHERE mobname = "Tenzen" AND mobid = 16908314;
UPDATE mob_spawn_points SET pos_x = 639.980, pos_y = -71.347, pos_z = -193.848, pos_rot = 65	WHERE mobname = "Tenzen" AND mobid = 16908318;

INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 4, 1, 255); -- Cure IV
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 21, 1, 255); -- Holy
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 34, 1, 255); -- Diaga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 39, 1, 255); -- Banishga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 46, 1, 255); -- Protect IV
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 50, 1, 255); -- Shell III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 56, 1, 255); -- Slow
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 57, 1, 255); -- Haste
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 58, 1, 255); -- Paralyze
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 59, 1, 255); -- Silence
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 111, 1, 255); -- Regen III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 112, 1, 255); -- Flash
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 146, 1, 255); -- Fire III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 151, 1, 255); -- Blizzard III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 161, 1, 255); -- Stone III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 166, 1, 255); -- Thunder III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 175, 1, 255); -- Firaga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 191, 1, 255); -- Stonega III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 195, 1, 255); -- Thundaga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 201, 1, 255); -- Waterga III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 226, 1, 255); -- Poisonga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 232, 1, 255); -- Bio III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 235, 1, 255); -- Burn
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 236, 1, 255); -- Frost
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 237, 1, 255); -- Choke
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 238, 1, 255); -- Rasp 
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 239, 1, 255); -- Shock
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 240, 1, 255); -- Drown
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 245, 1, 255); -- Drain
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 247, 1, 255); -- Aspir
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 258, 1, 255); -- Bind
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 274, 1, 255); -- Sleepga II

-- Further CoP6-4 Fixes
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Omega" AND mobid = 16908294;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Ultima" AND mobid = 16908295;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Omega" AND mobid = 16908301;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Ultima" AND mobid = 16908302;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Omega" AND mobid = 16908308;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Ultima" AND mobid = 16908309;

-- Promy & Spire Vahzl
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Propagator'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Procreator'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 200 WHERE NAME = 'Solicitor'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 200 WHERE NAME = 'Cumulator'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Ponderer'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Agonizer'; -- Faster attack speed to mimic era

-- Carbon Fishing Rod
UPDATE fishing_rod SET durability = 25 WHERE name = "Carbon Fishing Rod"; -- Increase durability to reduce the change of breaking on small fish

-- Citipati
UPDATE mob_groups SET HP = 8000 WHERE name = "Citipati"; -- Increase HP pool

-- Bastok 9-2
UPDATE mob_groups SET HP = 7800 WHERE name = "Zeid" AND zoneid = 165; -- Increase Zeid HP
UPDATE mob_groups SET HP = 500 WHERE name = "Shadow_of_Rage" AND zoneid = 165; -- Lower shadow of rage HP