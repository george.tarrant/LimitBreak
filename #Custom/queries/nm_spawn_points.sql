-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- GENERAL --
-- ----------


-- Giddeus
DELETE FROM nm_spawn_points WHERE mobid = 17371300 and pos = 5; -- Juu Duzu the Whirlwind - deleting wrong spawn location
DELETE FROM nm_spawn_points WHERE mobid = 17371515 and pos = 12; -- Hoo Mjuu the Torrent - deleting potentially problematic spawn position. Mob might clip into trees/rocks
DELETE FROM nm_spawn_points WHERE mobid = 17371515 and pos = 16; -- Hoo Mjuu the Torrent - deleting potentially problematic spawn position. Mob might clip into trees/rocks
DELETE FROM nm_spawn_points WHERE mobid = 17371515 and pos = 25; -- Hoo Mjuu the Torrent - deleting potentially problematic spawn position. Mob might clip into trees/rocks
DELETE FROM nm_spawn_points WHERE mobid = 17371515 and pos = 47; -- Hoo Mjuu the Torrent - deleting potentially problematic spawn position. Mob might clip into trees/rocks

-- Halvung
INSERT IGNORE INTO `nm_spawn_points` VALUES (17031401, 0, -236.129, 14.087, 287.822); -- Big Bomb
INSERT IGNORE INTO `nm_spawn_points` VALUES (17031401, 1, -239.087, 13.913, 308.361); -- Big Bomb
INSERT IGNORE INTO `nm_spawn_points` VALUES (17031401, 2, -196.793, 3.672, 296.014); -- Big Bomb
INSERT IGNORE INTO `nm_spawn_points` VALUES (17031401, 3, -177.828, -6.129, 282.548); -- Big Bomb

-- Quicksand Caves
INSERT IGNORE INTO `nm_spawn_points` VALUES (17629264,0,602.766,-5.921,-680.800); -- Adding Sabotender Bailarin

-- Shrine of Ru'Avitau
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 0, 719.330, -99.424, -600.401); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 1, 701.902, -100.000, -579.073); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 2, 725.422, -99.424, -569.653); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 3, 720.901, -100.022, -547.458); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 4, 755.819, -99.889, -549.613); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 5, 756.296, -99.424, -579.372); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 6, 775.812, -100.001, -585.723); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 7, 759.881, -99.744, -608.733); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 8, 742.210, -99.424, -596.418); -- Ullikummi
INSERT IGNORE INTO `nm_spawn_points` VALUES (17506418, 9, 736.745, -99.424, -579.979); -- Ullikummi

-- South Gustaberg
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 13, -360.824, 30.114, -454.615); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 14, -297.822, 23.327, -318.877); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 15, -288.541, 21.439, -330.873); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 16, -264.865, 20.765, -336.617); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 17, -332.532, 31.063, -381.882); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 18, -339.668, 28.619, -388.642); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 19, -321.228, 30.000, -382.849); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 20, -284.635, 20.810, -386.643); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 21, -333.047, 30.800, -325.419); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 22, -302.158, 27.208, -350.053); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 23, -315.889, 29.358, -318.021); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 24, -278.573, 20.138, -339.390); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 25, -293.311, 21.933, -318.758); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 26, -286.319, 20.645, -391.222); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 27, -322.615, 32.348, -417.432); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 28, -339.649, 32.626, -336.988); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 29, -336.543, 31.582, -341.736); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 30, -264.601, 22.994, -419.366); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 31, -278.841, 20.000, -362.440); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 32, -280.307, 20.035, -382.155); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 33, -368.938, 30.712, -429.955); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 34, -332.177, 30.313, -345.107); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 35, -330.438, 30.000, -327.495); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 36, -353.660, 29.043, -449.413); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 37, -323.998, 30.032, -404.350); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 38, -268.856, 21.258, -398.745); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 39, -264.791, 21.973, -374.422); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 40, -320.521, 29.886, -320.865); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 41, -355.270, 28.576, -463.004); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 42, -350.338, 32.823, -421.285); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 43, -263.350, 20.661, -331.257); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 44, -330.561, 30.093, -399.022); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 45, -273.274, 21.144, -347.048); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 46, -300.770, 26.288, -344.735); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 47, -318.258, 29.993, -347.883); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 48, -309.062, 28.860, -321.399); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 49, -317.119, 30.000, -322.596); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 50, -377.054, 36.182, -385.888); -- Leaping Lizzy (Added second LL to spawn tables)
INSERT IGNORE INTO `nm_spawn_points` VALUES(17215868, 51, -340.632, 34.587, -373.477); -- Leaping Lizzy (Added second LL to spawn tables)

-- Upper Delkfutts Tower
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 0, -322.953, -175.424, 0.762); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 1, -309.209, -175.424, 5.638); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 2, -294.361, -176.014, -13.924); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 3, -263.253, -176.000, 17.688); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 4, -276.544, -175.424, 62.725); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 5, -299.602, -175.424, 31.651); -- Alkyoneus

INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 0, -301.672, -159.424, 28.727); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 1, -317.560, -159.952, 51.111); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 2, -333.110, -160.019, 24.398); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 3, -307.259, -160.000, 20.870); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 4, -295.153, -159.424, -0.382); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 5, -259.163, -160.000, 23.274); -- Pallas

-- Wajaom Woodlands
UPDATE nm_spawn_points SET pos_x = -282.000, pos_y = -24.000, pos_z = -1.000 WHERE mobid = "16986355"; -- Hydra is back!

-- Zeruhn Mines
INSERT IGNORE INTO `nm_spawn_points` VALUES (17482751,0,60.425,8.735,-263.460); -- Adding Giant Amoeba NM

-- Sewer Syrup
DELETE FROM nm_spawn_points WHERE mobid = 17461307; -- Delete old spawn points
INSERT IGNORE INTO `nm_spawn_points` VALUES (17461307, 0, -21.04, 0.9251, -340.1736); -- Add new spawn point
