-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- Wild Rabbits
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186826";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186827";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186828";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186829";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186830";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186831";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186843";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186845";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186846";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186847";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186864";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17190939";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17190941";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17190945";

-- Tunnel Worms
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid = "17186832";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186833";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186834";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186835";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186836";
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17186838"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17186839"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17186840"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17186841"; 
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186854";
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17186857"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17186858"; 
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186869";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186870";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186872";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17186873";
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17186874"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17186875";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17190930";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17190932";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17190970";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211410";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211411";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211413";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211426";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211427";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211429";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211445";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211446";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211447";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211448";
UPDATE mob_level_exceptions SET maxLevel = "1" WHERE mobid = "17211449";
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362954"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362959"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362961"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362962"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362969"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362973"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362974"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362996"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17362997"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363001"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363002"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363003"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363004"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363005"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363006"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363010"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363011"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363021"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363022"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363023"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363024"; 
UPDATE mob_level_exceptions SET minLevel = "1", maxLevel = "1" WHERE mobid  = "17363025"; 





