-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -------------------------------
-- Adjusting skill ranks to ERA --
-- -------------------------------

-- Source: https://ffxiclopedia.fandom.com/wiki/Category:Combat_Skills?oldid=298667

-- values:
-- A+	1
-- A-	2
-- B+	3
-- B	4
-- B-	5
-- C+	6
-- C	7
-- C-	8
-- D	9
-- E	10
-- F	11

UPDATE skill_ranks SET war = 9, mnk = 1, whm = 0, blm = 0, rdm = 0, thf = 10, pld = 0, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 10, drg = 0, smn = 0, blu = 0, cor = 0, pup = 7 WHERE name = "hand2hand";
UPDATE skill_ranks SET war = 5, mnk = 0, whm = 0, blm = 9, rdm = 4, thf = 2, pld = 8, drk = 7, bst = 6, brd = 5, rng = 8, sam = 10, nin = 6, drg = 10, smn = 10, blu = 0, cor = 3, pup = 8 WHERE name = "dagger";
UPDATE skill_ranks SET war = 4, mnk = 0, whm = 0, blm = 0, rdm = 4, thf = 9, pld = 1, drk = 5, bst = 10, brd = 8, rng = 9, sam = 6, nin = 7, drg = 8, smn = 0, blu = 2, cor = 5, pup = 0 WHERE name = "sword";
UPDATE skill_ranks SET war = 3, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 4, drk = 2, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "great sword";
UPDATE skill_ranks SET war = 2, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 5, bst = 2, brd = 0, rng = 5, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "axe";
UPDATE skill_ranks SET war = 1, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 5, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "great axe";
UPDATE skill_ranks SET war = 3, mnk = 0, whm = 0, blm = 10, rdm = 0, thf = 0, pld = 0, drk = 1, bst = 5, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "scythe";
UPDATE skill_ranks SET war = 5, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 10, drk = 0, bst = 0, brd = 0, rng = 0, sam = 5, nin = 0, drg = 1, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "polearm";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 2, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "katana";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 0, rng = 0, sam = 1, nin = 8, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "great katana";
UPDATE skill_ranks SET war = 5, mnk = 6, whm = 3, blm = 6, rdm = 9, thf = 10, pld = 2, drk = 8, bst = 9, brd = 9, rng = 10, sam = 10, nin = 10, drg = 10, smn = 6, blu = 5, cor = 0, pup = 9 WHERE name = "club";
UPDATE skill_ranks SET war = 4, mnk = 4, whm = 6, blm = 5, rdm = 0, thf = 0, pld = 2, drk = 0, bst = 0, brd = 6, rng = 0, sam = 0, nin = 0, drg = 5, smn = 4, blu = 0, cor = 0, pup = 0 WHERE name = "staff";
UPDATE skill_ranks SET war = 9, mnk = 0, whm = 0, blm = 0, rdm = 9, thf = 8, pld = 0, drk = 0, bst = 0, brd = 0, rng = 2, sam = 6, nin = 10, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "archery";
UPDATE skill_ranks SET war = 9, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 6, pld = 0, drk = 10, bst = 0, brd = 0, rng = 2, sam = 0, nin = 7, drg = 0, smn = 0, blu = 0, cor = 4, pup = 0 WHERE name = "marksmanship";
UPDATE skill_ranks SET war = 9, mnk = 10, whm = 10, blm = 9, rdm = 11, thf = 9, pld = 0, drk = 0, bst = 0, brd = 10, rng = 8, sam = 7, nin = 2, drg = 0, smn = 0, blu = 0, cor = 6, pup = 6 WHERE name = "throwing";
UPDATE skill_ranks SET war = 0, mnk = 2, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 4 WHERE name = "guarding";
UPDATE skill_ranks SET war = 7, mnk = 3, whm = 10, blm = 10, rdm = 9, thf = 1, pld = 7, drk = 7, bst = 7, brd = 9, rng = 10, sam = 3, nin = 2, drg = 8, smn = 10, blu = 8, cor = 9, pup = 4 WHERE name = "evasion";
UPDATE skill_ranks SET war = 6, mnk = 0, whm = 9, blm = 0, rdm = 11, thf = 11, pld = 1, drk = 0, bst = 10, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "shield";
UPDATE skill_ranks SET war = 8, mnk = 10, whm = 0, blm = 0, rdm = 10, thf = 2, pld = 7, drk = 10, bst = 7, brd = 10, rng = 0, sam = 2, nin = 2, drg = 7, smn = 0, blu = 9, cor = 1, pup = 9 WHERE name = "parrying";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 2, blm = 0, rdm = 10, thf = 0, pld = 3, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "divine";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 1, blm = 0, rdm = 8, thf = 0, pld = 7, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "healing";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 6, blm = 10, rdm = 3, thf = 0, pld = 9, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "enhancing";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 7, blm = 6, rdm = 1, thf = 0, pld = 0, drk = 7, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "enfeebling";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 1, rdm = 6, thf = 0, pld = 0, drk = 3, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "elemental";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 2, rdm = 10, thf = 0, pld = 0, drk = 2, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "dark";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 2, blu = 0, cor = 0, pup = 0 WHERE name = "summoning";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 2, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "ninjutsu";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 7, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "singing";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 7, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "string";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 7, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 0, cor = 0, pup = 0 WHERE name = "wind";
UPDATE skill_ranks SET war = 0, mnk = 0, whm = 0, blm = 0, rdm = 0, thf = 0, pld = 0, drk = 0, bst = 0, brd = 0, rng = 0, sam = 0, nin = 0, drg = 0, smn = 0, blu = 1, cor = 0, pup = 0 WHERE name = "blue";