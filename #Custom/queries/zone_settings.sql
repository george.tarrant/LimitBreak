-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -------------------------
-- DISABLE NON-ToAU ZONES --
-- -------------------------
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Abyssea-Uleguerand";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Abyssea-Grauberg";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Abyssea-Empyreal_Paradox";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Western_Adoulin";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Eastern_Adoulin";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Rala_Waterways";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Rala_Waterways_U";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Yahse_Hunting_Grounds";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Ceizak_Battlegrounds";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Foret_de_Hennetiel";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Yorcia_Weald";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Yorcia_Weald_U";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Morimar_Basalt_Fields";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Marjami_Ravine";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Kamihr_Drifts";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Sih_Gates";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Moh_Gates";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Cirdas_Caverns";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Cirdas_Caverns_U";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Dho_Gates";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Woh_Gates";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Outer_RaKaznar";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Outer_RaKaznar_U";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "RaKaznar_Inner_Court";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "RaKaznar_Turris";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "278";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Walk_of_Echoes_[P2]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Mog_Garden";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Leafallia";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Mount_Kamihr";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Silver_Knife";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Celennia_Memorial_Library";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Feretory";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "286";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Maquette_Abdhaljs-Legion_B";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Escha_ZiTah";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Escha_RuAun";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Desuetia_Empyreal_Paradox";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Reisenjima";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Reisenjima_Henge";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Reisenjima_Sanctorium";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Dynamis-San_dOria_[D]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Dynamis-Bastok_[D]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Dynamis-Windurst_[D]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Dynamis-Jeuno_[D]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Walk_of_Echoes_[P1]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Abdhaljs Isle-Purgonorgo";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Southern_San_dOria_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "East_Ronfaure_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Jugner_Forest_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Vunkerl_Inlet_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Batallia_Downs_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "La_Vaule_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Everbloom_Hollow";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Bastok_Markets_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "North_Gustaberg_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Grauberg_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Pashhow_Marshlands_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Rolanberry_Fields_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Beadeaux_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Ruhotz_Silvermines";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Windurst_Waters_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "West_Sarutabaruta_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Fort_Karugo-Narugo_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Meriphataud_Mountains_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Sauromugue_Champaign_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Castle_Oztroja_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Ghoyus_Reverie";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Beaucedine_Glacier_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Xarcabard_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Castle_Zvahl_Baileys_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Castle_Zvahl_Keep_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Throne_Room_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Garlaige_Citadel_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Crawlers_Nest_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "The_Eldieme_Necropolis_[S]";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "Walk_of_Echoes";
UPDATE zone_settings SET zoneip = "0.0.0.0", zoneport = "0" WHERE name = "The_Colosseum"; -- Pankration