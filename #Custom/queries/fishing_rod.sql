-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- These will update the default values for NEW characters. Any characters that already have slots --
-- will be unaffected. THIS MUST BE DONE BEFORE ANY CHARACTERS ARE CREATED!!!!!!                   --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

UPDATE fishing_rod SET durability = 25 WHERE name = "Carbon Fishing Rod"; -- Increase durability to reduce the change of breaking on small fish
