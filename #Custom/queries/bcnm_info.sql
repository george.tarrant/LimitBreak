-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------


UPDATE bcnm_info SET zoneId = "139", name = "rank_2_mission", timeLimit ="1800", levelcap = "25", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 0; -- BCNM 0 is Rank 2-3 Dragon Fight - Horlaas Peak
UPDATE bcnm_info SET zoneId = "144", name = "rank_2_mission", timeLimit ="1800", levelcap = "25", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 64; -- BCNM 64 is Rank 2-3 Dragon Fight - Waughroon Shrine
UPDATE bcnm_info SET zoneId = "146", name = "rank_2_mission", timeLimit ="1800", levelcap = "25", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 96; -- BCNM 96 is Rank 2-3 Dragon Fight - Balgais Dais
UPDATE bcnm_info SET zoneId = "206", name = "rank_5_mission", timeLimit ="1800", levelcap = "50", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 512; -- BCNM 512 is Rank 5-1 Fei'yin Skele Fight
UPDATE bcnm_info SET zoneId = "165", name = "shadow_lord_battle", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 160; -- BCNM 160 is Rank 5-2 Shadow Lord Fight
UPDATE bcnm_info SET levelCap = "75" WHERE name = "shattering_stars"; -- level cap for Maat fights should be 75 and not 99

-- Bastok
UPDATE bcnm_info SET zoneId = "144", name = "on_my_way", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 67; -- BCNM 67 is Rank 7-2 On My Way
UPDATE bcnm_info SET zoneId = "165", name = "where_two_paths_converge", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 161; -- BCNM 161 is Rank 9-2 Where Two Paths Converge

-- Fei'Yin
UPDATE bcnm_info SET timeLimit = "900" WHERE name = "rank_5_mission"; -- 5.1 Rank mission should be 15 minutes time limit.

-- Jade Sepulcher
UPDATE bcnm_info SET rules = "22" WHERE bcnmID = 1154; -- BCNM 1154 is BLU LB5

-- San d'Oria
UPDATE bcnm_info SET zoneId = "140", name = "save_the_children", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 32; -- BCNM 32 is Rank 1-3 Save the Children
UPDATE bcnm_info SET zoneId = "139", name = "the_secret_weapon", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 3; -- BCNM 3 is Rank 7-2 The Secret Weapon
UPDATE bcnm_info SET zoneId = "206", name = "heir_to_the_light", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 516; -- BCNM 516 is Rank 9-2 The Heir to the Light

-- Windurst
UPDATE bcnm_info SET zoneId = "146", name = "saintly_invitation", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 99; -- BCNM 99 is Rank 6-2 Saintly Invitation
UPDATE bcnm_info SET zoneId = "170", name = "moon_reading", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 225; -- BCNM 225 is Rank 9-2 Moon Reading

-- Maat Fights
UPDATE bcnm_info SET rules = 20 WHERE name = "shattering_stars"; -- No Exp loss when you die to maat

-- -------------------------------------------------
-- Adding 75 level cap to all 75 cap battlefields --
-- -------------------------------------------------
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 83 AND name = "palborough_project";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 111 AND name = "seasons_greetings";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 112 AND name = "royale_ramble";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 114 AND name = "v_formation";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 115 AND name = "avian_apostates";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 677 AND name = "tango_with_a_tracker";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 610 AND name = "waking_the_beast";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 608 AND name = "trial_by_water";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 579 AND name = "waking_the_beast";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 132 AND name = "whom_wilt_thou_call";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 678 AND name = "requiem_of_a_sin";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 679 AND name = "antagonistic_ambuscade";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 84 AND name = "shell_shocked";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 802 AND name = "empty_dreams";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 113 AND name = "moa_constrictors";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 770 AND name = "empty_hopes";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 741 AND name = "mobline_comedy";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 739 AND name = "pulling_the_strings";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 738 AND name = "bionic_bug";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 706 AND name = "waking_dreams";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 546 AND name = "waking_the_beast";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 544 AND name = "trial_by_fire";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 533 AND name = "beyond_infinity";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 576 AND name = "trial_by_earth";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 480 AND name = "trial_by_ice";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 451 AND name = "waking_the_beast";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 449 AND name = "carbuncle_debacle";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 448 AND name = "trial_by_lightning";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 419 AND name = "waking_the_beast";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 417 AND name = "carbuncle_debacle";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 416 AND name = "trial_by_wind";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 354 AND name = "bonds_of_mythril";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 226 AND name = "waking_the_beast_fullmoon";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 483 AND name = "waking_the_beast";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 531 AND name = "clash_of_the_comrades";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 515 AND name = "infernal_swarm";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 514 AND name = "e-vase-ive_action";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 197 AND name = "cactuar_suave";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 198 AND name = "eye_of_the_storm";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 199 AND name = "scarlet_king";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 513 AND name = "come_into_my_parlor";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 201 AND name = "dragon_scales";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 224 AND name = "moonlit_path";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 352 AND name = "fiat_lux";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 82 AND name = "prehistoric_pigeons";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 107 AND name = "early_bird_catches_the_wyrm";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 15 AND name = "double_dragonian";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 16 AND name = "todays_horoscope";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 17 AND name = "contaminated_colosseum";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 18 AND name = "kindergarten_cap";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 20 AND name = "beyond_infinity";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 33 AND name = "holy_crest";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 19 AND name = "last_orc-shunned_hero";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 11 AND name = "horns_of_war";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 81 AND name = "operation_desert_swarm";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 76 AND name = "hills_are_alive";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 928 AND name = "ouryu_cometh";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 966 AND name = "uninvited_guests";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 967 AND name = "nest_of_nightmares";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 1122 AND name = "omens";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 896 AND name = "storms_of_fate";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 1155 AND name = "moment_of_truth";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 68 AND name = "thief_in_norg";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 866 AND name = "empty_aspirations";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 80 AND name = "copycat";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 1184 AND name = "rider_cometh";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 897 AND name = "wyrmking_descends";
UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID = 834 AND name = "empty_desires";


-- ---------------------
-- Rise of the Zilart --
-- ---------------------

-- ----------------------
-- Chains of Promathia --
-- ----------------------


-- --------------------------
-- Treasures of Aht Urghan --
-- --------------------------





-- Useful Queries

-- SELECT * FROM bcnm_info WHERE levelcap > 75; -- Shows list of all BCNM flagged for higher then 75 cap.  
-- UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID IN (X,X,X) -- List all the bcnmIDs identified in above query to change all BCNM level caps that are above 75 to 75 instead.
