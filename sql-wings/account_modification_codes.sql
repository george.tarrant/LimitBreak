-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 01, 2021 at 07:38 PM
-- Server version: 10.5.6-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `topaz`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_modification_codes`
--

DROP TABLE IF EXISTS `account_modification_codes`;
CREATE TABLE `account_modification_codes` (
  `account_id` INT(11) UNSIGNED NOT NULL,
  `verification_code` VARCHAR(50) NOT NULL DEFAULT '',
  `reset_password_code` VARCHAR(50) NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_modification_codes`
--
ALTER TABLE `account_modification_codes`
  ADD PRIMARY KEY (`account_id`);
COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
